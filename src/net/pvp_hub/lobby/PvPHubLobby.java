package net.pvp_hub.lobby;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.core.api.PluginMeta;
import net.pvp_hub.lobby.cmd.Fly;
import net.pvp_hub.lobby.cmd.GetLocation;
import net.pvp_hub.lobby.cmd.RaceBowFinish;
import net.pvp_hub.lobby.cmd.Spawn;
import net.pvp_hub.lobby.listener.BlockBreakListener;
import net.pvp_hub.lobby.listener.BlockPlaceListener;
import net.pvp_hub.lobby.listener.CreatureSpawnListener;
import net.pvp_hub.lobby.listener.FoodChangeListener;
import net.pvp_hub.lobby.listener.InventoryClickListener;
import net.pvp_hub.lobby.listener.MinecartListener;
import net.pvp_hub.lobby.listener.PlayerDamageListener;
import net.pvp_hub.lobby.listener.PlayerDeathListener;
import net.pvp_hub.lobby.listener.PlayerDisconnectListener;
import net.pvp_hub.lobby.listener.PlayerDropListener;
import net.pvp_hub.lobby.listener.PlayerInteractListener;
import net.pvp_hub.lobby.listener.PlayerJoinListener;
import net.pvp_hub.lobby.listener.PlayerMoveListener;
import net.pvp_hub.lobby.listener.PluginMessageListener;
import net.pvp_hub.lobby.listener.SignUpdateListener;
import net.pvp_hub.lobby.listener.WeatherChangeListener;
import net.pvp_hub.lobby.listener.WorldGuardListener;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Difficulty;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.CreatureType;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

import tk.theakio.sql.QueryResult;

public class PvPHubLobby extends JavaPlugin implements Listener {
	
	private QueryResult qr = null;
	public String main = "main";
	
	public ScoreboardManager manager = Bukkit.getScoreboardManager();
	public Scoreboard board = manager.getNewScoreboard();
	public Objective obj = board.registerNewObjective("side", "dummy");
	@SuppressWarnings("deprecation")
	public Score coins = obj.getScore(Bukkit.getOfflinePlayer("�aCoins"));
	@SuppressWarnings("deprecation")
	public Score chips = obj.getScore(Bukkit.getOfflinePlayer("�3Chips"));
	
	private static PvPHubLobby instance;
	
	public static PvPHubLobby getInstance() {
		return instance;
	}
	
	public PvPHubLobby() {
		instance = this;
	}
	
	public static ArrayList<String> hiders = new ArrayList<String>();
	public static ArrayList<Material> disallowedBlocks = new ArrayList<Material>();
	public static ArrayList<CreatureType> disallowedMobs = new ArrayList<CreatureType>();
	public static final List<String> antiSpam = new ArrayList();
	
	public static int antispamdelay = 1;
	
	public static ArrayList<Player> boxingArena = new ArrayList<Player>();
	public static HashMap<ItemStack[], Player> baInventory = new HashMap<ItemStack[], Player>();
	
	public static ArrayList<Player> schwatteWolle = new ArrayList<Player>();
	public static ArrayList<Player> raceBow = new ArrayList<Player>();
	public static ArrayList<Player> blockJump = new ArrayList<Player>();
	
    public static File locFile = new File("plugins/PvPHubLobby", "Locations.yml");
    public static FileConfiguration locCfg = YamlConfiguration.loadConfiguration(locFile);
    
    public static boolean allowEmerald;
    public static boolean allowHide;
    public static boolean allowSettings;
    public static boolean allowSpawn;

	public void onEnable(){
		Bukkit.getServer().getPluginManager().registerEvents(this, this);
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerJoinListener(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new InventoryClickListener(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerInteractListener(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerDisconnectListener(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerDamageListener(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new WeatherChangeListener(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new FoodChangeListener(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new CreatureSpawnListener(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerMoveListener(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new BlockBreakListener(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new SignUpdateListener(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerDropListener(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new BlockPlaceListener(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new MinecartListener(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerDeathListener(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new WorldGuardListener(this), this);
		
		allowEmerald = true;
		allowHide = true;
		allowSettings = true;
		allowSpawn = true;
		
		// /getlocation
		getCommand("getlocation").setExecutor(new GetLocation(this));
		
		// /rbfinish
		getCommand("rbfinish").setExecutor(new RaceBowFinish(this));
				
		// /rbfinish
		getCommand("rbcount").setExecutor(new RaceBowFinish(this));
		
		// /fly
		getCommand("fly").setExecutor(new Fly(this));
		
		// /spawn
		getCommand("spawn").setExecutor(new Spawn(this));
				
		// /spawn
		getCommand("teamlobby").setExecutor(new Spawn(this));
		
		System.out.println("[Lobby] Registering Plugin Channels..."); {
			this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
			PluginMessageListener pml = new PluginMessageListener();
	        this.getServer().getMessenger().registerIncomingPluginChannel(this, "BungeeCord", pml);
		}
       
        Bukkit.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        
        disallowedBlocks.add(Material.BEACON);
        disallowedBlocks.add(Material.TRAP_DOOR);
        disallowedBlocks.add(Material.FURNACE);
        disallowedBlocks.add(Material.BURNING_FURNACE);
        disallowedBlocks.add(Material.CHEST);
        disallowedBlocks.add(Material.DISPENSER);
        disallowedBlocks.add(Material.DROPPER);
        disallowedBlocks.add(Material.HOPPER);
        disallowedBlocks.add(Material.HOPPER_MINECART);
        disallowedBlocks.add(Material.WORKBENCH);
        disallowedBlocks.add(Material.BREWING_STAND);
        
        disallowedMobs.add(CreatureType.SILVERFISH);
        disallowedMobs.add(CreatureType.CREEPER);
        disallowedMobs.add(CreatureType.ENDERMAN);
        disallowedMobs.add(CreatureType.ZOMBIE);
        disallowedMobs.add(CreatureType.SKELETON);
        disallowedMobs.add(CreatureType.BLAZE);
        disallowedMobs.add(CreatureType.CAVE_SPIDER);
        disallowedMobs.add(CreatureType.SPIDER);
        disallowedMobs.add(CreatureType.ENDER_DRAGON);
        disallowedMobs.add(CreatureType.GHAST);
        disallowedMobs.add(CreatureType.GIANT);
        disallowedMobs.add(CreatureType.MAGMA_CUBE);
        disallowedMobs.add(CreatureType.PIG_ZOMBIE);
        //disallowedMobs.add(CreatureType.BAT);
        
        for(World w : Bukkit.getWorlds()){
        	w.setDifficulty(Difficulty.PEACEFUL);
        }
	}
	
    /*@EventHandler(priority = EventPriority.HIGHEST)
    public void onNameTag(AsyncPlayerReceiveNameTagEvent event) {
    	if (event.getNamedPlayer().getName().equals("Zeryther")) {
    		event.setTag("�4Zeryther");
    	}
    	
    	if (event.getNamedPlayer().getName().equals("Notch")) {
    		event.setTag("�eNotch");
    	}
    }
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onReceiveNameTag(AsyncPlayerReceiveNameTagEvent e){
		if(e.getNamedPlayer().getName() == "Zeryther"){
			e.setTag("�4Zeryther");
		} else {
			e.setTag(e.getNamedPlayer().getPlayerListName());
		}
	}*/
	
	@EventHandler(priority=EventPriority.HIGHEST)
	  public void onDestroyByEntity(HangingBreakByEntityEvent event)
	  {
	    String msg1 = PluginMeta.prefix + "Keine Rechte";
	    if ((event.getRemover() instanceof Player))
	    {
	      Player p = (Player)event.getRemover();
	      if ((event.getEntity().getType() == EntityType.ITEM_FRAME) && 
	        (!p.isOp()) && (!p.hasPermission("frame.remove")))
	      {
	        p.sendMessage(ChatColor.DARK_RED + msg1);
	        event.setCancelled(true);
	        return;
	      }
	    }
	  }
	  
	  @EventHandler(priority=EventPriority.HIGHEST)
	  public void OnPlaceByEntity(HangingPlaceEvent event)
	  {
	    String msg2 = PluginMeta.prefix + "Keine Rechte";
	    Player p = event.getPlayer();
	    if ((event.getEntity().getType() == EntityType.ITEM_FRAME) && 
	      (!p.isOp()) && (!p.hasPermission("frame.place")))
	    {
	      p.sendMessage(ChatColor.DARK_RED + msg2);
	      event.setCancelled(true);
	      return;
	    }
	  }
	  
	  @EventHandler
	  public void canRotate(PlayerInteractEntityEvent event)
	  {
	    String msg3 = PluginMeta.prefix + "Keine Rechte";
	    Entity entity = event.getRightClicked();
	    Player player = event.getPlayer();
	    if (!entity.getType().equals(EntityType.ITEM_FRAME)) {
	      return;
	    }
	    ItemFrame iFrame = (ItemFrame)entity;
	    if ((iFrame.getItem().equals(null)) || (iFrame.getItem().getType().equals(Material.AIR))) {
	      return;
	    }
	    if ((!player.isOp()) && (!player.hasPermission("frame.rotate")))
	    {
	      player.sendMessage(ChatColor.DARK_RED + msg3);
	      event.setCancelled(true);
	    }
	  }
	  
	  @EventHandler
	  public void ItemRemoval(EntityDamageByEntityEvent e)
	  {
	    String msg4 = PluginMeta.prefix + "Keine Rechte";
	    if ((e.getDamager() instanceof Player))
	    {
	      Player p = (Player)e.getDamager();
	      if ((e.getEntity().getType() == EntityType.ITEM_FRAME) && 
	        (!p.isOp()) && (!p.hasPermission("frame.item.remove")))
	      {
	        p.sendMessage(ChatColor.DARK_RED + msg4);
	        e.setCancelled(true);
	      }
	    }
	    if (((e.getDamager() instanceof Projectile)) && (e.getEntity().getType() == EntityType.ITEM_FRAME))
	    {
	      Projectile p = (Projectile)e.getDamager();
	      Player player = (Player)p.getShooter();
	      if ((!player.isOp()) && (!player.hasPermission("frame.item.remove")))
	      {
	        player.sendMessage(ChatColor.DARK_RED + msg4);
	        e.setCancelled(true);
	      }
	    }
	  }
	
	public static void useHidingItem(final Player toTarget){
		ArrayList<String> gloLore = new ArrayList<String>();
		gloLore.add("�r�7Verstecke andere Spieler in der Lobby.");
		gloLore.add("�r�cTeam-Mitglieder und YouTuber siehst du trotzdem!");
		
		ItemStack hid = new ItemStack(Material.NETHER_STAR);
		ItemMeta hidM = hid.getItemMeta();
		hidM.setDisplayName("�e�l-=- Spieler verstecken -=-");
		hidM.setLore(gloLore);
		hid.setItemMeta(hidM);
		
		ItemStack sho = new ItemStack(Material.NETHER_STAR);
		ItemMeta shoM = sho.getItemMeta();
		shoM.setDisplayName("�8�l-=- Spieler anzeigen -=-");
		shoM.setLore(gloLore);
		sho.setItemMeta(shoM);
		
		if (!antiSpam.contains(toTarget.getName()))
	    {
	      if (hiders.contains(toTarget.getName()))
	      {
	        hiders.remove(toTarget.getName());
	        toTarget.sendMessage(PluginMeta.prefix + "�aDu siehst jetzt wieder alle Spieler.");
	        for (Player p : Bukkit.getOnlinePlayers()) {
	          if (p != null) {
	            toTarget.showPlayer(p);
	          }
	        }
	        toTarget.getInventory().setItem(7, hid);
	        antiSpam.add(toTarget.getName());
	        new BukkitRunnable()
	        {
	          public void run()
	          {
	            PvPHubLobby.antiSpam.remove(toTarget.getName());
	          }
	        }.runTaskLater(instance, antispamdelay * 20L);
	      }
	      else
	      {
	        hiders.add(toTarget.getName());
	        toTarget.sendMessage(PluginMeta.prefix + "�aDu siehst jetzt keine Spieler mehr.");
	        for (Player p : Bukkit.getOnlinePlayers()) {
	        	try {
					if(PlayerUtilities.getRank(p.getName()) > 3 || PlayerUtilities.getRank(p.getName()) == 3 && (p != null)){
						toTarget.hidePlayer(p);
					    toTarget.getInventory().setItem(7, sho);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        }
	        antiSpam.add(toTarget.getName());
	        new BukkitRunnable()
	        {
	          public void run()
	          {
	        	  PvPHubLobby.antiSpam.remove(toTarget.getName());
	          }
	        }.runTaskLater(instance, antispamdelay * 20L);
	      }
	    }
	    else {
	      toTarget.sendMessage(PluginMeta.prefix + "�cBitte warte einen kurzen Moment.");
	    }
	}
	
	public void onDisable(){ }
	
	@EventHandler
	public static void onBlockRedstone(BlockRedstoneEvent e) {
		if (e.getBlock().getType() == Material.REDSTONE_LAMP_OFF || e.getBlock().getType() == Material.REDSTONE_LAMP_ON) {
			e.setNewCurrent(5);
		}
	}
	
}
