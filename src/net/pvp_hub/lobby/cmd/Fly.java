package net.pvp_hub.lobby.cmd;

import net.pvp_hub.core.PvPHubCore;
import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.lobby.PvPHubLobby;
import net.pvp_hub.lobby.api.PluginMeta;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Fly implements CommandExecutor {

	private PvPHubLobby plugin;
	public Fly(PvPHubLobby plugin){
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("fly")){
			if((sender instanceof Player)){
				Player p = (Player)sender;
				try {
					if(PlayerUtilities.getRank(p.getName()) > 2){
						if(p.isFlying()){
							p.setAllowFlight(false);
							p.setFlying(false);
							p.sendMessage(PluginMeta.prefix + "�aDu kannst nicht mehr fliegen.");
						} else {
							p.setAllowFlight(true);
							p.setFlying(true);
							p.sendMessage(PluginMeta.prefix + "�aDu kannst jetzt fliegen.");
						}
					} else {
						p.sendMessage(PluginMeta.noperms);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				sender.sendMessage(PluginMeta.noplayer);
			}
		}
		
		return false;
	}
	
}
