package net.pvp_hub.lobby.cmd;

import net.pvp_hub.lobby.PvPHubLobby;
import net.pvp_hub.lobby.api.PluginMeta;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GetLocation implements CommandExecutor {

	private PvPHubLobby plugin;
	public GetLocation(PvPHubLobby plugin){
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("getlocation")){
			if((sender instanceof Player)){
				Player p = (Player)sender;
				Location loc = p.getLocation();
				double x = loc.getX();
				double y = loc.getY();
				double z = loc.getZ();
				
				float yaw = loc.getYaw();
				float pitch = loc.getPitch();
				
				p.sendMessage(PluginMeta.prefix + "x: �f" + x);
				p.sendMessage(PluginMeta.prefix + "y: �f" + y);
				p.sendMessage(PluginMeta.prefix + "z: �f" + z);
				p.sendMessage(PluginMeta.prefix + "yaw: �f" + yaw);
				p.sendMessage(PluginMeta.prefix + "pitch: �f" + pitch);
			}
		}
		
		return false;
	}
	
}
