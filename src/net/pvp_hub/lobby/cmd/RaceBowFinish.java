package net.pvp_hub.lobby.cmd;

import net.pvp_hub.lobby.PvPHubLobby;
import net.pvp_hub.lobby.api.PluginMeta;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.CommandBlock;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RaceBowFinish implements CommandExecutor {

	private PvPHubLobby plugin;
	public RaceBowFinish(PvPHubLobby plugin){
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("rbfinish")){
			if(sender.isOp()){
				if(args.length == 0){
					sender.sendMessage(PluginMeta.prefix + "�c/rbfinish <Player>");
				} else {
					for(Player all : plugin.raceBow){
						all.sendMessage(PluginMeta.RaceBowPrefix + "�a" + args[0] + " �r�7hat das Ziel erreicht!");
					}
				}
			} else {
				sender.sendMessage(PluginMeta.noperms);
			}
		}
		
		if(cmd.getName().equalsIgnoreCase("rbcount")){
			if(sender.isOp()){
				if(args.length == 0){
					sender.sendMessage(PluginMeta.prefix + "�c/rbcount <Count>");
				} else {
					for(Player all : plugin.raceBow){
						all.sendMessage(PluginMeta.RaceBowPrefix + "�e" + args[0]);
					}
				}
			} else {
				sender.sendMessage(PluginMeta.noperms);
			}
		}
		
		return false;
	}
	
}
