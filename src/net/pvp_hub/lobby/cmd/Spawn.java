package net.pvp_hub.lobby.cmd;

import net.pvp_hub.core.PvPHubCore;
import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.lobby.PvPHubLobby;
import net.pvp_hub.lobby.api.Locations;
import net.pvp_hub.lobby.api.PluginMeta;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Spawn implements CommandExecutor {

	private PvPHubLobby plugin;
	public Spawn(PvPHubLobby plugin){
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("spawn")){
			if((sender instanceof Player)){
				Player p = (Player)sender;
				p.teleport(Locations.spawn);
				p.sendMessage(PluginMeta.prefix + "�aWoosh!");
			} else {
				sender.sendMessage(PluginMeta.noplayer);
			}
		}
		
		if(cmd.getName().equalsIgnoreCase("teamlobby")){
			if((sender instanceof Player)){
				Player p = (Player)sender;
				try {
					if(PlayerUtilities.getRank(p.getName()) >= 5){
						p.teleport(Locations.teamSpawn);
						p.sendMessage(PluginMeta.prefix + "�aWoosh!");
					} else {
						p.sendMessage(PluginMeta.noperms);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				sender.sendMessage(PluginMeta.noplayer);
			}
		}
		
		return false;
	}
	
}
