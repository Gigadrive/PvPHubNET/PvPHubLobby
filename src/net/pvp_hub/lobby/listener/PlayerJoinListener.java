package net.pvp_hub.lobby.listener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import net.pvp_hub.core.PvPHubCore;
import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.core.language.Language;
import net.pvp_hub.lobby.PvPHubLobby;
import net.pvp_hub.lobby.api.Locations;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.potion.PotionEffect;

public class PlayerJoinListener implements Listener {

	private PvPHubLobby plugin;
	public PlayerJoinListener(PvPHubLobby plugin){
		this.plugin = plugin;
	}
	
	public HashMap<UUID, PermissionAttachment> sgchest = new HashMap<UUID, PermissionAttachment>();
	
	@EventHandler
	public void onJoin(final PlayerJoinEvent e){
		final Player p = e.getPlayer();
		e.setJoinMessage(null);
		
		
		
		try {
			if(PlayerUtilities.getRank(p.getName()) >= 5 && PlayerUtilities.hasTeamSpawn(p) == 1){
				p.teleport(Locations.teamSpawn);
			} else {
				p.teleport(Locations.spawn);
			}
		} catch (Exception e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		
		// Give Achievement
		try {
			PlayerUtilities.achieve(1, p);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//PlayerUtilities.clearInventory(p);
		
		for (PotionEffect effect : e.getPlayer().getActivePotionEffects()) {
			e.getPlayer().removePotionEffect(effect.getType());
		}
		
		try {
			if(PlayerUtilities.getRank(p.getName()) > 3 || PlayerUtilities.getRank(p.getName()) == 3){
				for(String a : plugin.hiders){
					Player all = Bukkit.getPlayer(a);
					all.hidePlayer(p);
				}
			}
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		ItemStack em = new ItemStack(Material.EMERALD, 1);
		ItemMeta emMeta = em.getItemMeta();
		try {
			emMeta.setDisplayName("�2�l-=- Spielmodi -=-");
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		ArrayList<String> emLore = new ArrayList<String>();
		emLore.add("�r�7Zeigt dir eine Liste der Spielmodi");
		emMeta.setLore(emLore);
		em.setItemMeta(emMeta);
		
		ItemStack glo = new ItemStack(Material.NETHER_STAR, 1);
		ItemMeta gloMeta = glo.getItemMeta();
		gloMeta.setDisplayName("�e�l-=- Spieler verstecken -=-");
		ArrayList<String> gloLore = new ArrayList<String>();
		gloLore.add("�r�7Verstecke andere Spieler in der Lobby.");
		gloLore.add("�r�cTeam-Mitglieder und YouTuber siehst du trotzdem!");
		gloMeta.setLore(gloLore);
		glo.setItemMeta(gloMeta);
		
		ItemStack ma = new ItemStack(Material.MAGMA_CREAM, 1);
		ItemMeta maMeta = ma.getItemMeta();
		maMeta.setDisplayName("�c�l-=- Spawn -=-");
		ArrayList<String> maLore = new ArrayList<String>();
		maLore.add("�r�7Teleportiere dich zum Spawn.");
		maMeta.setLore(maLore);
		ma.setItemMeta(maMeta);
		
		ItemStack bo = new ItemStack(Material.BOOK, 1);
		ItemMeta boMeta = bo.getItemMeta();
		boMeta.setDisplayName("�4�l-=- Admin Tools -=-");
		ArrayList<String> boLore = new ArrayList<String>();
		boLore.add("�r�7Zeigt die erhaltenen Errungenschaften an.");
		boMeta.setLore(boLore);
		bo.setItemMeta(boMeta);
		
		ItemStack qua = new ItemStack(Material.QUARTZ, 1);
		ItemMeta quaMeta = qua.getItemMeta();
		quaMeta.setDisplayName("�9�l-=- Einstellungen -=-");
		ArrayList<String> quaLore = new ArrayList<String>();
		quaLore.add("�r�7Treffe Einstellungen f�r die Lobby/den Server");
		quaMeta.setLore(quaLore);
		qua.setItemMeta(quaMeta);
		
		try {
			if(PlayerUtilities.getRank(p.getName()) >= 9){
				p.getInventory().setItem(8, bo); // Admin Tools
				p.getInventory().setItem(7, ma); // Magma Cream
				p.getInventory().setItem(6, glo); // Nether Star
				p.getInventory().setItem(5, qua); // Quartz
				p.getInventory().setItem(4, em); // Emerald
			} else {
				p.getInventory().setItem(8, ma); // Magma Cream
				p.getInventory().setItem(7, glo); // Nether Star
				p.getInventory().setItem(6, qua); // Quartz
				p.getInventory().setItem(5, em); // Emerald
			}
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		PermissionAttachment attachment = p.addAttachment(plugin);
		attachment.setPermission("teleportsigns.use", true);
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, 
				
				new Runnable(){
			
					public void run(){
						try {
							String joinMsg = p.getDisplayName() + " �6hat die Lobby betreten.";
							if(PlayerUtilities.hasAutoNick(p) == 1){
								p.performCommand("rnick");
								joinMsg = null;
							}
							
							if(PlayerUtilities.getRank(p.getName()) != 0){
								if(joinMsg != null){
									Bukkit.broadcastMessage(joinMsg);
								}
							}
							
							p.sendMessage("�2Willkommen auf PvP-Hub.net!");
							p.sendMessage("�eVielen Dank f�r's Teilnehmen an unserem Beta-Test.");
							p.sendMessage("�eBitte bedenke, dass es noch einige Bugs gibt und du uns");
							p.sendMessage("�ebeim Fixen helfen kannst, indem du Bugs meldest.");
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
		
				}
				
		);
	}
	
}
