package net.pvp_hub.lobby.listener;

import net.pvp_hub.lobby.PvPHubLobby;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

public class FoodChangeListener implements Listener {

	private PvPHubLobby plugin;
	public FoodChangeListener(PvPHubLobby plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onChangeWeather(FoodLevelChangeEvent e){
		Player p = (Player)e.getEntity();
		
		e.setCancelled(true);
		p.setFoodLevel(20);
	}
	
}
