package net.pvp_hub.lobby.listener;

import net.pvp_hub.lobby.PvPHubLobby;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

public class PlayerDropListener implements Listener {

	private PvPHubLobby plugin;
	public PlayerDropListener(PvPHubLobby plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onDrop(PlayerDropItemEvent e){
		Player p = e.getPlayer();
		
		if(!p.isOp()){
			e.setCancelled(true);
		} else {
			e.setCancelled(false);
		}
	}
	
}
