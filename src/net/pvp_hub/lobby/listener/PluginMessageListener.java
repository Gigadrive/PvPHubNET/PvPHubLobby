package net.pvp_hub.lobby.listener;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class PluginMessageListener implements org.bukkit.plugin.messaging.PluginMessageListener {

	@Override
	public void onPluginMessageReceived(String channel, Player player, byte[] message) {
		if (!channel.equals("BungeeCord")) {
			return;
		}
		 
		DataInputStream in = new DataInputStream(new ByteArrayInputStream(message));
		String subchannel = null;
		try {
			subchannel = in.readUTF();
		} catch (IOException e) {
			System.err.println("[Lobby] An Error Occured while trying to read the Subchannel (" + e.getMessage() + ")");
		}
		if(subchannel.equals("TeleportToMain")) {
			String sp = null;
			try {
				sp = in.readUTF();
			} catch (IOException e) {
				System.err.println("[Lobby] Invalid Input for Subchannel 'TeleportToMain' (" + e.getMessage() + ")");
			}
			
			Player p = Bukkit.getPlayer(sp);
			if(p != null) {
				p.teleport(new Location(Bukkit.getWorld("world"), 288, 8, 152));
				p.sendMessage("�4[PvP-Hub] �6Wilkommen zur�ck auf der Main Hub!");
			}
		}
		else {
			System.err.println("[Lobby] Invalid Plugin Message Received with Subchannel '" + subchannel + "'");
		}
	}
	
}
