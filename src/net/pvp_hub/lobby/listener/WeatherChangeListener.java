package net.pvp_hub.lobby.listener;

import net.pvp_hub.lobby.PvPHubLobby;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.weather.WeatherChangeEvent;

public class WeatherChangeListener implements Listener {

	private PvPHubLobby plugin;
	public WeatherChangeListener(PvPHubLobby plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onChangeWeather(WeatherChangeEvent e){
		e.setCancelled(true);
		System.out.println("[LOGGER] Cancelled WeatherChangeEvent");
	}
	
}
