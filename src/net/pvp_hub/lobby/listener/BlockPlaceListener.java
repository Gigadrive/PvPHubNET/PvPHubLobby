package net.pvp_hub.lobby.listener;

import net.pvp_hub.lobby.PvPHubLobby;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

public class BlockPlaceListener implements Listener {

	private PvPHubLobby plugin;
	public BlockPlaceListener(PvPHubLobby plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onPlace(BlockPlaceEvent e){
		Player p = e.getPlayer();
		
		if(p.isOp()){
			e.setCancelled(false);
		} else {
			e.setCancelled(true);
		}
	}
	
}
