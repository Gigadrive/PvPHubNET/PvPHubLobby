package net.pvp_hub.lobby.listener;

import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.lobby.PvPHubLobby;
import net.pvp_hub.lobby.api.Locations;
import net.pvp_hub.lobby.api.PluginMeta;
import net.pvp_hub.lobby.inv.AdminTools;
import net.pvp_hub.lobby.inv.BuyItemMenu;
import net.pvp_hub.lobby.inv.LS_Clothes;
import net.pvp_hub.lobby.inv.LS_DM;
import net.pvp_hub.lobby.inv.LS_Heads;
import net.pvp_hub.lobby.inv.LS_Main;
import net.pvp_hub.lobby.inv.LS_SPVP;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

public class InventoryClickListener implements Listener {

	private PvPHubLobby plugin;
	public InventoryClickListener(PvPHubLobby plugin){
		this.plugin = plugin;
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onClick(InventoryClickEvent e){
		final Player p = (Player) e.getWhoClicked();
		try  {
			if(e.getInventory().getName().equals("            �2-=- Spielmodi -=-")){
				if(e.getCurrentItem().getType().equals(Material.CHEST)){
					p.closeInventory();
					p.teleport(Locations.survGames);
					p.sendMessage(PluginMeta.prefix + "�aDu wurdest zu �4Survival Games�r �ateleportiert.");
				} else if(e.getCurrentItem().getType().equals(Material.ROTTEN_FLESH)){
					p.closeInventory();
					p.teleport(Locations.infWars);
					p.sendMessage(PluginMeta.prefix + "�aDu wurdest zu �2Infection Wars�r �ateleportiert.");
				} else if(e.getCurrentItem().getType().equals(Material.DIAMOND_CHESTPLATE)){
					p.closeInventory();
					p.teleport(Locations.deathMatch);
					p.sendMessage(PluginMeta.prefix + "�aDu wurdest zu �3Deathmatch�r �ateleportiert.");
				} else if(e.getCurrentItem().getType().equals(Material.MUSHROOM_SOUP)){
					p.closeInventory();
					p.teleport(Locations.sPvP);
					p.sendMessage(PluginMeta.prefix + "�aDu wurdest zu �eSoupPvP�r �ateleportiert.");
				} else if(e.getCurrentItem().getType().equals(Material.LEATHER_BOOTS)){
					p.closeInventory();
					p.teleport(Locations.raceBow);
					p.sendMessage(PluginMeta.prefix + "�aDu wurdest zu �5RaceBow�r �ateleportiert.");
				} else if(e.getCurrentItem().getType().equals(Material.FEATHER)){
					p.closeInventory();
					p.teleport(Locations.blockJump);
					p.sendMessage(PluginMeta.prefix + "�aDu wurdest zu �4Block�eJump�r �ateleportiert.");
				} else if(e.getCurrentItem().getType().equals(Material.BED)){
					p.closeInventory();
					p.sendMessage(PluginMeta.soon);
				} else if(e.getCurrentItem().getType().equals(Material.BOW)){
					p.closeInventory();
					p.sendMessage(PluginMeta.soon);
				} else if(e.getCurrentItem().getType().equals(Material.OBSIDIAN)){
					p.closeInventory();
					p.sendMessage(PluginMeta.soon);
				} else if(e.getCurrentItem().getType().equals(Material.DIAMOND_PICKAXE)){
					p.closeInventory();
					p.sendMessage(PluginMeta.soon);
				} else if(e.getCurrentItem().getType().equals(Material.MAGMA_CREAM)){
					p.closeInventory();
					p.teleport(Locations.spawn);
					p.sendMessage(PluginMeta.prefix + "�aDu wurdest zum �cSpawn�r �ateleportiert.");
				} else {
					e.setCancelled(true);
					p.closeInventory();
				}
			}
		} catch(Exception e1){}
		
		try  {
			if(e.getInventory().getName().equals("Lobby-Shop")){
				if(e.getCurrentItem().getType().equals(Material.MUSHROOM_SOUP)){
					e.setCancelled(true);
					p.closeInventory();
					LS_SPVP.openFor(p);
				} else if(e.getCurrentItem().getType().equals(Material.MONSTER_EGG)){
					e.setCancelled(true);
					p.sendMessage(PluginMeta.soon);
				} else if(e.getCurrentItem().getType().equals(Material.ROTTEN_FLESH)){
					e.setCancelled(true);
					p.sendMessage(PluginMeta.soon);
				} else if(e.getCurrentItem().getType().equals(Material.LEATHER_CHESTPLATE)){
					e.setCancelled(true);
					p.closeInventory();
					LS_Clothes.openFor(p);
				} else if(e.getCurrentItem().getType().equals(Material.DIAMOND_CHESTPLATE)){
					e.setCancelled(true);
					p.closeInventory();
					LS_DM.openFor(p);
				} else if(e.getCurrentItem().getType().equals(Material.SKULL_ITEM)){
					e.setCancelled(true);
					p.closeInventory();
					LS_Heads.openFor(p);
				} else {
					e.setCancelled(true);
					p.closeInventory();
				}
			}
		} catch(Exception e1){ e1.printStackTrace(); }
		
		try {
			if(e.getInventory().getName().equals("�6H�te")){
				if(e.getCurrentItem().getItemMeta().getDisplayName().equals("�7Kopf von �eZeryther")){
					e.setCancelled(true);
					p.closeInventory();
					
					ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
				    SkullMeta meta = (SkullMeta)skull.getItemMeta();
				    meta.setOwner("Zeryther");
				    skull.setItemMeta(meta);
				    
				    p.getInventory().setHelmet(skull);
				    p.sendMessage(PluginMeta.prefix + "�aDu tr�gst nun den Kopf von �e" + "Zeryther");
				} else if(e.getCurrentItem().getItemMeta().getDisplayName().equals("�7Kopf von �eVipsNiceMinePlay")){
					e.setCancelled(true);
					p.closeInventory();
					
					ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
				    SkullMeta meta = (SkullMeta)skull.getItemMeta();
				    meta.setOwner("VipsNiceMinePlay");
				    skull.setItemMeta(meta);
				    
				    p.getInventory().setHelmet(skull);
				    p.sendMessage(PluginMeta.prefix + "�aDu tr�gst nun den Kopf von �e" + "VipsNiceMinePlay");
				} else if(e.getCurrentItem().getItemMeta().getDisplayName().equals("�7Kopf von �ecrafter14360")){
					e.setCancelled(true);
					p.closeInventory();
					
					ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
				    SkullMeta meta = (SkullMeta)skull.getItemMeta();
				    meta.setOwner("crafter14360");
				    skull.setItemMeta(meta);
				    
				    p.getInventory().setHelmet(skull);
				    p.sendMessage(PluginMeta.prefix + "�aDu tr�gst nun den Kopf von �e" + "crafter14360");
				} else if(e.getCurrentItem().getItemMeta().getDisplayName().equals("�7Kopf von einem �eH�hnchen")){
					e.setCancelled(true);
					p.closeInventory();
					
					ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
				    SkullMeta meta = (SkullMeta)skull.getItemMeta();
				    meta.setOwner("MHF_Chicken");
				    skull.setItemMeta(meta);
				    
				    p.getInventory().setHelmet(skull);
				    p.sendMessage(PluginMeta.prefix + "�aDu tr�gst nun den Kopf von einem �e" + "H�hnchen");
				} else if(e.getCurrentItem().getItemMeta().getDisplayName().equals("�7Kopf von einem �eSchwein")){
					e.setCancelled(true);
					p.closeInventory();
					
					ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
				    SkullMeta meta = (SkullMeta)skull.getItemMeta();
				    meta.setOwner("MHF_Pig");
				    skull.setItemMeta(meta);
				    
				    p.getInventory().setHelmet(skull);
				    p.sendMessage(PluginMeta.prefix + "�aDu tr�gst nun den Kopf von einem �e" + "Schwein");
				} else if(e.getCurrentItem().getItemMeta().getDisplayName().equals("�c�lHut absetzen")){
					e.setCancelled(true);
					p.closeInventory();
					
					p.getInventory().setHelmet(null);
					
				    p.sendMessage(PluginMeta.prefix + "�aDu tr�gst nun keinen Hut mehr.");
				} else {
					e.setCancelled(true);
					p.closeInventory();
				}
			}
		} catch(Exception e1){}
		
		try  {
			if(e.getInventory().getName().equals("�eSoupPvP-Shop")){
				if(e.getCurrentItem().getItemMeta().getDisplayName().equals("�5�lWizard")){
					e.setCancelled(true);
					p.closeInventory();
					BuyItemMenu.openFor(p, "�5�lWizard");
				} else if(e.getCurrentItem().getItemMeta().getDisplayName().equals("�3�lSurvivor")){
					e.setCancelled(true);
					p.closeInventory();
					BuyItemMenu.openFor(p, "�3�lSurvivor");
				} else if(e.getCurrentItem().getItemMeta().getDisplayName().equals("�b�lHeavy")){
					e.setCancelled(true);
					p.closeInventory();
					BuyItemMenu.openFor(p, "�b�lHeavy");
				} else {
					e.setCancelled(true);
					p.closeInventory();
				}
			}
		} catch(Exception e1){ e1.printStackTrace(); }
		
		try  {
			if(e.getInventory().getName().equals("�3DeathMatch-Shop")){
				if(e.getCurrentItem().getItemMeta().getDisplayName().equals("�7�lLederr�stung �a(Starterkit)")){
					e.setCancelled(true);
					p.closeInventory();
					BuyItemMenu.openFor(p, "�7�lLederr�stung �a(Starterkit)");
				} else {
					e.setCancelled(true);
					p.closeInventory();
				}
			}
		} catch(Exception e1){ e1.printStackTrace(); }
		
		try  {
			if(e.getInventory().getName().equals("�3�lSurvivor")){
				if(e.getCurrentItem().getType().equals(Material.REDSTONE)){
					e.setCancelled(true);
					p.closeInventory();
				} else if(e.getCurrentItem().getType().equals(Material.NAME_TAG)){
					e.setCancelled(true);
					p.closeInventory();
					if(PlayerUtilities.getChips(p) < PlayerUtilities.getChipsForItem(18)){
						p.sendMessage(PluginMeta.prefix + "�cDu hast nicht genug �6Chips�c.");
					} else {
						PlayerUtilities.removeChips(p, PlayerUtilities.getChipsForItem(18));
						p.closeInventory();
						p.sendMessage(PluginMeta.prefix + "�aDu hast das �3�lSurvivor-Kit �r�af�r �eSoupPvP �aerhalten.");
						p.playSound(p.getEyeLocation(), Sound.LEVEL_UP, 1.0F, 1.0F);
						PlayerUtilities.buyShopItem(p, 18);
					}
				} else if(e.getCurrentItem().getType().equals(Material.GOLD_NUGGET)){
					e.setCancelled(true);
					p.closeInventory();
					if(PlayerUtilities.getCoins(p) < PlayerUtilities.getCoinsForItem(18)){
						p.sendMessage(PluginMeta.prefix + "�cDu hast nicht genug Coins.");
					} else {
						PlayerUtilities.removeCoins(p, PlayerUtilities.getCoinsForItem(18));
						p.closeInventory();
						p.sendMessage(PluginMeta.prefix + "�aDu hast das �3�lSurvivor-Kit �r�af�r �eSoupPvP �aerhalten.");
						p.playSound(p.getEyeLocation(), Sound.LEVEL_UP, 1.0F, 1.0F);
						PlayerUtilities.buyShopItem(p, 18);
					}
				} else {
					e.setCancelled(true);
					p.closeInventory();
				}
			}
		} catch(Exception e1){ e1.printStackTrace(); }
		
		try  {
			if(e.getInventory().getName().equals("�5�lWizard")){
				if(e.getCurrentItem().getType().equals(Material.REDSTONE)){
					e.setCancelled(true);
					p.closeInventory();
				} else if(e.getCurrentItem().getType().equals(Material.NAME_TAG)){
					e.setCancelled(true);
					p.closeInventory();
					if(PlayerUtilities.getChips(p) < PlayerUtilities.getChipsForItem(17)){
						p.sendMessage(PluginMeta.prefix + "�cDu hast nicht genug �6Chips�c.");
					} else {
						PlayerUtilities.removeChips(p, PlayerUtilities.getChipsForItem(17));
						p.closeInventory();
						p.sendMessage(PluginMeta.prefix + "�aDu hast das �5�lWizard-Kit �r�af�r �eSoupPvP �aerhalten.");
						p.playSound(p.getEyeLocation(), Sound.LEVEL_UP, 1.0F, 1.0F);
						PlayerUtilities.buyShopItem(p, 17);
					}
				} else if(e.getCurrentItem().getType().equals(Material.GOLD_NUGGET)){
					e.setCancelled(true);
					p.closeInventory();
					if(PlayerUtilities.getCoins(p) < PlayerUtilities.getCoinsForItem(17)){
						p.sendMessage(PluginMeta.prefix + "�cDu hast nicht genug Coins.");
					} else {
						PlayerUtilities.removeCoins(p, PlayerUtilities.getCoinsForItem(17));
						p.closeInventory();
						p.sendMessage(PluginMeta.prefix + "�aDu hast das �5�lWizard-Kit �r�af�r �eSoupPvP �aerhalten.");
						p.playSound(p.getEyeLocation(), Sound.LEVEL_UP, 1.0F, 1.0F);
						PlayerUtilities.buyShopItem(p, 17);
					}
				} else {
					e.setCancelled(true);
					p.closeInventory();
				}
			}
		} catch(Exception e1){}
		
		try  {
			if(e.getInventory().getName().equals("�b�lHeavy")){
				if(e.getCurrentItem().getType().equals(Material.REDSTONE)){
					e.setCancelled(true);
					p.closeInventory();
				} else if(e.getCurrentItem().getType().equals(Material.NAME_TAG)){
					e.setCancelled(true);
					p.closeInventory();
					if(PlayerUtilities.getChips(p) < PlayerUtilities.getChipsForItem(19)){
						p.sendMessage(PluginMeta.prefix + "�cDu hast nicht genug �6Chips�c.");
					} else {
						PlayerUtilities.removeChips(p, PlayerUtilities.getChipsForItem(19));
						p.closeInventory();
						p.sendMessage(PluginMeta.prefix + "�aDu hast das �b�lHeavy-Kit �r�af�r �eSoupPvP �aerhalten.");
						p.playSound(p.getEyeLocation(), Sound.LEVEL_UP, 1.0F, 1.0F);
						PlayerUtilities.buyShopItem(p, 19);
					}
				} else if(e.getCurrentItem().getType().equals(Material.GOLD_NUGGET)){
					e.setCancelled(true);
					p.closeInventory();
					if(PlayerUtilities.getCoins(p) < PlayerUtilities.getCoinsForItem(19)){
						p.sendMessage(PluginMeta.prefix + "�cDu hast nicht genug Coins.");
					} else {
						PlayerUtilities.removeCoins(p, PlayerUtilities.getCoinsForItem(19));
						p.closeInventory();
						p.sendMessage(PluginMeta.prefix + "�aDu hast das �b�lHeavy-Kit �r�af�r �eSoupPvP �aerhalten.");
						p.playSound(p.getEyeLocation(), Sound.LEVEL_UP, 1.0F, 1.0F);
						PlayerUtilities.buyShopItem(p, 19);
					}
				} else {
					e.setCancelled(true);
					p.closeInventory();
				}
			}
		} catch(Exception e1){ e1.printStackTrace(); }
		
		try  {
			if(e.getInventory().getName().equals("�7�lLederr�stung �a(Starterkit)")){
				if(e.getCurrentItem().getType().equals(Material.REDSTONE)){
					e.setCancelled(true);
					p.closeInventory();
				} else if(e.getCurrentItem().getType().equals(Material.NAME_TAG)){
					e.setCancelled(true);
					p.closeInventory();
					if(PlayerUtilities.getChips(p) < PlayerUtilities.getChipsForItem(20)){
						p.sendMessage(PluginMeta.prefix + "�cDu hast nicht genug �6Chips�c.");
					} else {
						PlayerUtilities.removeChips(p, PlayerUtilities.getChipsForItem(20));
						p.closeInventory();
						p.sendMessage(PluginMeta.prefix + "�aDu hast das �7�lLeder-Kit �r�af�r �3DeathMatch �aerhalten.");
						p.playSound(p.getEyeLocation(), Sound.LEVEL_UP, 1.0F, 1.0F);
						PlayerUtilities.buyShopItem(p, 20);
					}
				} else if(e.getCurrentItem().getType().equals(Material.GOLD_NUGGET)){
					e.setCancelled(true);
					p.closeInventory();
					if(PlayerUtilities.getCoins(p) < PlayerUtilities.getCoinsForItem(20)){
						p.sendMessage(PluginMeta.prefix + "�cDu hast nicht genug Coins.");
					} else {
						PlayerUtilities.removeCoins(p, PlayerUtilities.getCoinsForItem(20));
						p.closeInventory();
						p.sendMessage(PluginMeta.prefix + "�aDu hast das �b�lHeavy-Kit �r�af�r �3DeathMatch �aerhalten.");
						p.playSound(p.getEyeLocation(), Sound.LEVEL_UP, 1.0F, 1.0F);
						PlayerUtilities.buyShopItem(p, 20);
					}
				} else {
					e.setCancelled(true);
					p.closeInventory();
				}
			}
		} catch(Exception e1){ e1.printStackTrace(); }
		
		try  {
			if(e.getInventory().getName().equals("�4Admin Tools")){
				if(e.getCurrentItem().getType().equals(Material.REDSTONE_BLOCK)){
					e.setCancelled(true);
					p.closeInventory();
					Bukkit.reload();
				} else if(e.getCurrentItem().getType().equals(Material.DIAMOND_PICKAXE)){
					e.setCancelled(true);
					p.closeInventory();
					AdminTools.openFor(p, "gamemode");
				} else if(e.getCurrentItem().getType().equals(Material.WATCH)){
					e.setCancelled(true);
					p.closeInventory();
					AdminTools.openFor(p, "time");
				} else if(e.getCurrentItem().getType().equals(Material.NETHER_BRICK_ITEM)){
					e.setCancelled(true);
					p.closeInventory();
					p.performCommand("vanish");
				} else if(e.getCurrentItem().getType().equals(Material.EMERALD)){
					if(PvPHubLobby.allowEmerald){
						e.setCancelled(true);
						p.closeInventory();
						PvPHubLobby.allowEmerald = true;
						for(Player op : Bukkit.getOnlinePlayers()){
							if(op.isOp()){
								op.sendMessage(PluginMeta.sn_prefix + "�aDas �2Spielmodi �aItem wurde von " + op.getDisplayName() + " �aaktiviert.");
							}
						}
					} else {
						e.setCancelled(true);
						p.closeInventory();
						PvPHubLobby.allowEmerald = false;
						for(Player op : Bukkit.getOnlinePlayers()){
							if(op.isOp()){
								op.sendMessage(PluginMeta.sn_prefix + "�cDas �2Spielmodi �cItem wurde von " + op.getDisplayName() + " �cdeaktiviert.");
							}
						}
					}
				} else if(e.getCurrentItem().getType().equals(Material.NETHER_STAR)){
					if(PvPHubLobby.allowSettings){
						e.setCancelled(true);
						p.closeInventory();
						PvPHubLobby.allowHide = true;
						for(Player op : Bukkit.getOnlinePlayers()){
							if(op.isOp()){
								op.sendMessage(PluginMeta.sn_prefix + "�aDas �eSpieler verstecken �aItem wurde von " + op.getDisplayName() + " �aaktiviert.");
							}
						}
					} else {
						e.setCancelled(true);
						p.closeInventory();
						PvPHubLobby.allowHide = false;
						for(Player op : Bukkit.getOnlinePlayers()){
							if(op.isOp()){
								op.sendMessage(PluginMeta.sn_prefix + "�cDas �eSpieler verstecken �cItem wurde von " + op.getDisplayName() + " �cdeaktiviert.");
							}
						}
					}
				} else if(e.getCurrentItem().getType().equals(Material.QUARTZ)){
					if(PvPHubLobby.allowSettings){
						e.setCancelled(true);
						p.closeInventory();
						PvPHubLobby.allowSettings = true;
						for(Player op : Bukkit.getOnlinePlayers()){
							if(op.isOp()){
								op.sendMessage(PluginMeta.sn_prefix + "�aDas �9Einstellungen �aItem wurde von " + op.getDisplayName() + " �aaktiviert.");
							}
						}
					} else {
						e.setCancelled(true);
						p.closeInventory();
						PvPHubLobby.allowSettings = false;
						for(Player op : Bukkit.getOnlinePlayers()){
							if(op.isOp()){
								op.sendMessage(PluginMeta.sn_prefix + "�cDas �9Einstellungen �cItem wurde von " + op.getDisplayName() + " �cdeaktiviert.");
							}
						}
					}
				} else if(e.getCurrentItem().getType().equals(Material.MAGMA_CREAM)){
					if(PvPHubLobby.allowSpawn){
						e.setCancelled(true);
						p.closeInventory();
						PvPHubLobby.allowSpawn = true;
						for(Player op : Bukkit.getOnlinePlayers()){
							if(op.isOp()){
								op.sendMessage(PluginMeta.sn_prefix + "�aDas �cSpawn �aItem wurde von " + op.getDisplayName() + " �aaktiviert.");
							}
						}
					} else {
						e.setCancelled(true);
						p.closeInventory();
						PvPHubLobby.allowSpawn = false;
						for(Player op : Bukkit.getOnlinePlayers()){
							if(op.isOp()){
								op.sendMessage(PluginMeta.sn_prefix + "�cDas Spawn Item wurde von " + op.getDisplayName() + " �cdeaktiviert.");
							}
						}
					}
				} else {
					e.setCancelled(true);
					p.closeInventory();
				}
			}
		} catch(Exception e1){}
		
		try  {
			if(e.getInventory().getName().equals("�4Zeit �ndern")){
				if(e.getCurrentItem().getItemMeta().getDisplayName().equals("�bMorgen")){
					e.setCancelled(true);
					p.closeInventory();
					p.getWorld().setTime(0L);
				} else if(e.getCurrentItem().getItemMeta().getDisplayName().equals("�6Mittag")){
					e.setCancelled(true);
					p.closeInventory();
					p.getWorld().setTime(6000L);
				} else if(e.getCurrentItem().getItemMeta().getDisplayName().equals("�9Abend")){
					e.setCancelled(true);
					p.closeInventory();
					p.getWorld().setTime(23000L);
				} else {
					e.setCancelled(true);
					p.closeInventory();
				}
			}
		} catch(Exception e1){}
		
		try  {
			if(e.getInventory().getName().equals("�4Gamemode �ndern")){
				if(e.getCurrentItem().getType().equals(Material.IRON_BLOCK)){
					e.setCancelled(true);
					p.closeInventory();
					p.performCommand("gm 0");
				} else if(e.getCurrentItem().getType().equals(Material.GOLD_BLOCK)){
					e.setCancelled(true);
					p.closeInventory();
					p.performCommand("gm 2");
				} else if(e.getCurrentItem().getType().equals(Material.DIAMOND_BLOCK)){
					e.setCancelled(true);
					p.closeInventory();
					p.performCommand("gm 1");
				} else {
					e.setCancelled(true);
					p.closeInventory();
				}
			}
		} catch(Exception e1){}
		
		try  {
			if(e.getInventory().getName().equals("�5JukeBox")){
				if(e.getCurrentItem().getItemMeta().getDisplayName().equals("�6�l13")){
					e.setCancelled(true);
					p.sendMessage(PluginMeta.prefix + "�5JukeBox �8� " + e.getCurrentItem().getItemMeta().getDisplayName());
					p.playEffect(p.getLocation(), Effect.RECORD_PLAY, 2256);
				} else if(e.getCurrentItem().getItemMeta().getDisplayName().equals("�a�lCat")){
					e.setCancelled(true);
					p.sendMessage(PluginMeta.prefix + "�5JukeBox �8� " + e.getCurrentItem().getItemMeta().getDisplayName());
					p.playEffect(p.getLocation(), Effect.RECORD_PLAY, 2257);
				} else if(e.getCurrentItem().getItemMeta().getDisplayName().equals("�c�lBlocks")){
					e.setCancelled(true);
					p.sendMessage(PluginMeta.prefix + "�5JukeBox �8� " + e.getCurrentItem().getItemMeta().getDisplayName());
					p.playEffect(p.getLocation(), Effect.RECORD_PLAY, 2258);
				} else if(e.getCurrentItem().getItemMeta().getDisplayName().equals("�4�lChirp")){
					e.setCancelled(true);
					p.sendMessage(PluginMeta.prefix + "�5JukeBox �8� " + e.getCurrentItem().getItemMeta().getDisplayName());
					p.playEffect(p.getLocation(), Effect.RECORD_PLAY, 2259);
				} else if(e.getCurrentItem().getItemMeta().getDisplayName().equals("�a�lFar")){
					e.setCancelled(true);
					p.sendMessage(PluginMeta.prefix + "�5JukeBox �8� " + e.getCurrentItem().getItemMeta().getDisplayName());
					p.playEffect(p.getLocation(), Effect.RECORD_PLAY, 2260);
				} else if(e.getCurrentItem().getItemMeta().getDisplayName().equals("�5�lMall")){
					e.setCancelled(true);
					p.sendMessage(PluginMeta.prefix + "�5JukeBox �8� " + e.getCurrentItem().getItemMeta().getDisplayName());
					p.playEffect(p.getLocation(), Effect.RECORD_PLAY, 2261);
				} else if(e.getCurrentItem().getItemMeta().getDisplayName().equals("�d�lMellohi")){
					e.setCancelled(true);
					p.sendMessage(PluginMeta.prefix + "�5JukeBox �8� " + e.getCurrentItem().getItemMeta().getDisplayName());
					p.playEffect(p.getLocation(), Effect.RECORD_PLAY, 2262);
				} else if(e.getCurrentItem().getItemMeta().getDisplayName().equals("�8�lStal")){
					e.setCancelled(true);
					p.sendMessage(PluginMeta.prefix + "�5JukeBox �8� " + e.getCurrentItem().getItemMeta().getDisplayName());
					p.playEffect(p.getLocation(), Effect.RECORD_PLAY, 2263);
				} else if(e.getCurrentItem().getItemMeta().getDisplayName().equals("�f�lStrad")){
					e.setCancelled(true);
					p.sendMessage(PluginMeta.prefix + "�5JukeBox �8� " + e.getCurrentItem().getItemMeta().getDisplayName());
					p.playEffect(p.getLocation(), Effect.RECORD_PLAY, 2264);
				} else if(e.getCurrentItem().getItemMeta().getDisplayName().equals("�2�lWard")){
					e.setCancelled(true);
					p.sendMessage(PluginMeta.prefix + "�5JukeBox �8� " + e.getCurrentItem().getItemMeta().getDisplayName());
					p.playEffect(p.getLocation(), Effect.RECORD_PLAY, 2265);
				} else if(e.getCurrentItem().getItemMeta().getDisplayName().equals("�8�l11")){
					e.setCancelled(true);
					p.sendMessage(PluginMeta.prefix + "�5JukeBox �8� " + e.getCurrentItem().getItemMeta().getDisplayName());
					p.playEffect(p.getLocation(), Effect.RECORD_PLAY, 2266);
				} else if(e.getCurrentItem().getItemMeta().getDisplayName().equals("�b�lWait")){
					e.setCancelled(true);
					p.sendMessage(PluginMeta.prefix + "�5JukeBox �8� " + e.getCurrentItem().getItemMeta().getDisplayName());
					p.playEffect(p.getLocation(), Effect.RECORD_PLAY, 2267);
				} else {
					e.setCancelled(true);
					p.closeInventory();
				}
			}
		} catch(Exception e1){}
		
		try  {
			if(e.getInventory().getName().equals("Kleiderschrank")){
				if(e.getCurrentItem().getType().equals(Material.DIAMOND)){
					e.setCancelled(true);
					p.closeInventory();
					LS_Main.openFor(p);
				} else if(e.getCurrentItem().getType().equals(Material.STAINED_GLASS_PANE)){
					e.setCancelled(true);
					p.closeInventory();
				} else if(e.getCurrentItem().getType().equals(Material.REDSTONE_BLOCK)){
					e.setCancelled(true);
					p.closeInventory();
					p.getInventory().setArmorContents(null);
					p.sendMessage(PluginMeta.prefix + "�aDu hast dich ausgezogen.");
				} else {
					e.setCancelled(true);
					p.sendMessage(PluginMeta.prefix + "�aDu tr�gst nun " + e.getCurrentItem().getType().toString());
					
					//p.getInventory().setContents(new ItemStack(e.getCurrentItem().getType()));
					
					if(e.getCurrentItem().getType().toString().contains("HELMET")){
						p.getInventory().setHelmet(new ItemStack(e.getCurrentItem().getType()));
					} else if(e.getCurrentItem().getType().toString().contains("CHESTPLATE")){
						p.getInventory().setChestplate(new ItemStack(e.getCurrentItem().getType()));
					} else if(e.getCurrentItem().getType().toString().contains("LEGGINGS")){
						p.getInventory().setLeggings(new ItemStack(e.getCurrentItem().getType()));
					} else if(e.getCurrentItem().getType().toString().contains("BOOTS")){
						p.getInventory().setBoots(new ItemStack(e.getCurrentItem().getType()));
					} else {
						p.getInventory().setHelmet(new ItemStack(e.getCurrentItem().getType()));
					}
				}
			}
		} catch(Exception e1){}
		
		try {
			if(e.getInventory().getName().equals("�9Einstellungen")){
				if(e.getCurrentItem().getType().equals(Material.NAME_TAG)){
					if(PlayerUtilities.hasAutoNick(p) == 0){
						PlayerUtilities.updateSetting(p, "autoNick", true);
						e.setCancelled(true);
						p.closeInventory();
						p.sendMessage(PluginMeta.prefix + "�aDu wirst nun in der Lobby automatisch genickt!");
					} else {
						PlayerUtilities.updateSetting(p, "autoNick", false);
						e.setCancelled(true);
						p.closeInventory();
						p.sendMessage(PluginMeta.prefix + "�aDu wirst nun nicht mehr in der Lobby automatisch genickt!");
					}
				} else if(e.getCurrentItem().getType().equals(Material.BOOK)){
					if(PlayerUtilities.hasShowStats(p) == 0){
						PlayerUtilities.updateSetting(p, "showStats", true);
						e.setCancelled(true);
						p.closeInventory();
						p.sendMessage(PluginMeta.prefix + "�aAndere k�nnen nun deine Stats sehen!");
					} else {
						PlayerUtilities.updateSetting(p, "showStats", false);
						e.setCancelled(true);
						p.closeInventory();
						p.sendMessage(PluginMeta.prefix + "�aAndere k�nnen nun nicht mehr deine Stats sehen!");
					}
				} else if(e.getCurrentItem().getType().equals(Material.SKULL_ITEM)){
					if(PlayerUtilities.hasFriends(p) == 0){
						PlayerUtilities.updateSetting(p, "allowFriendRequests", true);
						e.setCancelled(true);
						p.closeInventory();
						p.sendMessage(PluginMeta.prefix + "�aAndere k�nnen dir nun Freundschaftsanfragen schicken!");
					} else {
						PlayerUtilities.updateSetting(p, "allowFriendRequests", false);
						e.setCancelled(true);
						p.closeInventory();
						p.sendMessage(PluginMeta.prefix + "�aAndere k�nnen dir nun keine Freundschaftsanfragen mehr schicken!");
					}
				} else if(e.getCurrentItem().getType().equals(Material.REDSTONE)){
					if(PlayerUtilities.hasTeamSpawn(p) == 0){
						PlayerUtilities.updateSetting(p, "teamSpawn", true);
						e.setCancelled(true);
						p.closeInventory();
						p.sendMessage(PluginMeta.prefix + "�aDu spawnst nun beim Login beim Team-Spawn!");
					} else {
						PlayerUtilities.updateSetting(p, "teamSpawn", false);
						e.setCancelled(true);
						p.closeInventory();
						p.sendMessage(PluginMeta.prefix + "�aDu spawnst nun nicht mehr beim Login beim Team-Spawn!");
					}
				} else if(e.getCurrentItem().getType().equals(Material.GHAST_TEAR)){
					if(PlayerUtilities.hasServerLoc(p) == 0){
						PlayerUtilities.updateSetting(p, "serverLocation", true);
						e.setCancelled(true);
						p.closeInventory();
						p.sendMessage(PluginMeta.prefix + "�aAndere k�nnen nun deinen Server sehen!");
					} else {
						PlayerUtilities.updateSetting(p, "serverLocation", false);
						e.setCancelled(true);
						p.closeInventory();
						p.sendMessage(PluginMeta.prefix + "�aAndere k�nnen nun nicht mehr deinen Server sehen!");
					}
				} else {
					e.setCancelled(true);
					p.closeInventory();
				}
			}
		} catch(Exception e1){}
		
		try {
			if(e.getInventory().getName().equals("Zeryther")){
				if(e.getCurrentItem().getType().equals(Material.COOKIE)){
					if(PlayerUtilities.getCoins(p) > 20){
						PlayerUtilities.removeCoins(p, 20);
						p.getInventory().addItem(e.getCurrentItem());
					} else {
						p.sendMessage(PluginMeta.prefix + "�cDu hast nicht genug Coins.");
					}
					e.setCancelled(true);
					p.closeInventory();
				}
				
				if(e.getCurrentItem().getType().equals(Material.APPLE)){
					if(PlayerUtilities.getCoins(p) > 80){
						PlayerUtilities.removeCoins(p, 80);
						p.getInventory().addItem(e.getCurrentItem());
					} else {
						p.sendMessage(PluginMeta.prefix + "�cDu hast nicht genug Coins.");
					}
					e.setCancelled(true);
					p.closeInventory();
				}
				
				if(e.getCurrentItem().getType().equals(Material.COOKED_BEEF)){
					if(PlayerUtilities.getCoins(p) > 95){
						PlayerUtilities.removeCoins(p, 95);
						p.getInventory().addItem(e.getCurrentItem());
					} else {
						p.sendMessage(PluginMeta.prefix + "�cDu hast nicht genug Coins.");
					}
					e.setCancelled(true);
					p.closeInventory();
				}
			}
			
			if(e.getInventory().getName().equals("rex2go")){
				if(e.getCurrentItem().getType().equals(Material.POISONOUS_POTATO)){
					if(PlayerUtilities.getCoins(p) > 5){
						PlayerUtilities.removeCoins(p, 5);
						p.getInventory().addItem(e.getCurrentItem());
					} else {
						p.sendMessage(PluginMeta.prefix + "�cDu hast nicht genug Coins.");
					}
					e.setCancelled(true);
					p.closeInventory();
				}
			}
			
			if(e.getInventory().getName().equals("Materia__")){
				
			}
			
			if(e.getInventory().getName().equals("VipsNiceMinePlay")){
				if(e.getCurrentItem().getType().equals(Material.CHEST)){
					if(PlayerUtilities.getChips(p) > 1){
						PlayerUtilities.removeChips(p, 1);
						p.getInventory().addItem(new ItemStack(Material.GOLD_INGOT));
						p.sendMessage(PluginMeta.prefix + "�aTrololo! Dieser Goldbarren bringt dir nichts. Das ist nur ein Troll =P");
					} else {
						p.sendMessage(PluginMeta.prefix + "�cDu hast nicht genug Coins.");
					}
					e.setCancelled(true);
					p.closeInventory();
				}
				
				if(e.getCurrentItem().getType().equals(Material.POTION)){
					if(PlayerUtilities.getCoins(p) > 25){
						PlayerUtilities.removeCoins(p, 25);
						p.getInventory().addItem(e.getCurrentItem());
					} else {
						p.sendMessage(PluginMeta.prefix + "�cDu hast nicht genug Coins.");
					}
					e.setCancelled(true);
					p.closeInventory();
				}
				
				if(e.getCurrentItem().getType().equals(Material.BREAD)){
					if(PlayerUtilities.getCoins(p) > 30){
						PlayerUtilities.removeCoins(p, 30);
						p.getInventory().addItem(e.getCurrentItem());
					} else {
						p.sendMessage(PluginMeta.prefix + "�cDu hast nicht genug Coins.");
					}
					e.setCancelled(true);
					p.closeInventory();
				}
			}
			
			if(e.getInventory().getName().equals("herthaner2_0")){
				if(e.getCurrentItem().getType().equals(Material.MINECART)){
					if(PlayerUtilities.getCoins(p) > 25){
						PlayerUtilities.removeCoins(p, 25);
						p.getInventory().addItem(e.getCurrentItem());
					} else {
						p.sendMessage(PluginMeta.prefix + "�cDu hast nicht genug Coins.");
					}
					e.setCancelled(true);
					p.closeInventory();
				}
			}
		} catch(Exception e1){}
	}
		
}
