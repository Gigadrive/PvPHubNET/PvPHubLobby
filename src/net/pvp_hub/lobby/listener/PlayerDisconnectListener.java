package net.pvp_hub.lobby.listener;

import net.pvp_hub.core.PvPHubCore;
import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.lobby.PvPHubLobby;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerDisconnectListener implements Listener {

	private PvPHubLobby plugin;
	public PlayerDisconnectListener(PvPHubLobby plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onJoin(PlayerQuitEvent e){
		Player p = e.getPlayer();
		
		e.setQuitMessage(null);
		
		String joinMsg = p.getDisplayName() + " �6hat die Lobby verlassen.";
		try {
			if(PlayerUtilities.hasAutoNick(p) == 1){
				joinMsg = null;
			}
			
			if(PlayerUtilities.getRank(p.getName()) != 0){
				if(joinMsg != null){
					Bukkit.broadcastMessage(joinMsg);
				}
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		plugin.hiders.remove(p.getName());
	}
	
}
