package net.pvp_hub.lobby.listener;

import net.pvp_hub.lobby.PvPHubLobby;

import org.bukkit.entity.CreatureType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;

public class CreatureSpawnListener implements Listener {

	private PvPHubLobby plugin;
	public CreatureSpawnListener(PvPHubLobby plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onChangeWeather(CreatureSpawnEvent e){
		CreatureType ent = e.getCreatureType();
		
		if(PvPHubLobby.disallowedMobs.contains(ent)){
			e.setCancelled(true);
		}
	}
	
}
