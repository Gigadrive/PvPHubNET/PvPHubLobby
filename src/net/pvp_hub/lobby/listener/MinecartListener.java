package net.pvp_hub.lobby.listener;

import net.pvp_hub.lobby.PvPHubLobby;
import net.pvp_hub.lobby.api.Locations;

import org.bukkit.Bukkit;
import org.bukkit.entity.Minecart;
import org.bukkit.entity.Player;
import org.bukkit.entity.Vehicle;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.vehicle.VehicleEnterEvent;
import org.bukkit.event.vehicle.VehicleExitEvent;

public class MinecartListener implements Listener {

	private PvPHubLobby plugin;
	public MinecartListener(PvPHubLobby plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onLeave(VehicleExitEvent e){
		Vehicle v = e.getVehicle();
		Player p = null;
		
		if(e.getExited() instanceof Player){
			p = (Player)e.getExited();
		}
		
		if(v instanceof Minecart && p != null){
			final Player pp = p;
			v.remove();
			Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
				public void run(){
					pp.teleport(Locations.minecartStart);
				}
			});
		}
	}
	
	@EventHandler
	public void onEnter(VehicleEnterEvent e){
		Vehicle v = e.getVehicle();
		Player p = null;
		Minecart m = null;
		
		if(e.getEntered() instanceof Player){
			p = (Player)e.getEntered();
		}
		
		if(v instanceof Minecart && p != null){
			m = (Minecart)v;
		}
	}
	
}
