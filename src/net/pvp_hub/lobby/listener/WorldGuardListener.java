package net.pvp_hub.lobby.listener;

import java.util.Map.Entry;

import net.pvp_hub.core.PvPHubCore;
import net.pvp_hub.core.api.PluginMeta;
import net.pvp_hub.lobby.PvPHubLobby;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;

import com.mewin.WGRegionEvents.events.RegionEnterEvent;
import com.mewin.WGRegionEvents.events.RegionLeaveEvent;

public class WorldGuardListener implements Listener {

	private PvPHubLobby plugin;
	public WorldGuardListener(PvPHubLobby plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onEnter(RegionEnterEvent e){
		Player p = e.getPlayer();
		
		if(e.getRegion().getId().equalsIgnoreCase("BoxArena")){
			if(!plugin.boxingArena.contains(p)){
				plugin.boxingArena.add(p);
			}
			
			plugin.baInventory.put(p.getInventory().getContents(), p);
			p.getInventory().clear();
			
			for(Player all : plugin.boxingArena){
				all.sendMessage(PluginMeta.prefix + p.getDisplayName() + " �ahat die Box-Arena betreten!");
			}
			
			p.setCanPickupItems(false);
		}
		
		if(e.getRegion().getId().equalsIgnoreCase("Trampolin")){
			plugin.schwatteWolle.add(p);
		}
		
		if(e.getRegion().getId().equalsIgnoreCase("RaceBow")){
			plugin.raceBow.add(p);
		}
		
		if(e.getRegion().getId().equalsIgnoreCase("BlockJump")){
			plugin.blockJump.add(p);
			
			for(Player all : plugin.blockJump){
				all.sendMessage(PluginMeta.prefix + p.getDisplayName() + " �ahat den BlockJump betreten!");
			}
		}
	}
	
	@EventHandler
	public void onLeave(RegionLeaveEvent e){
		Player p = e.getPlayer();
		
		if(e.getRegion().getId().equalsIgnoreCase("BoxArena")){
			
			ItemStack[] actualInventory = null;
			for (Entry<ItemStack[], Player> entry : plugin.baInventory.entrySet()) {
	            if (entry.getValue().equals(p)) {
	                actualInventory = entry.getKey();
	            }
	        }
			
			if(actualInventory != null){
				p.getInventory().setContents(actualInventory);
			}
			
			plugin.baInventory.remove(p);
			
			for(Player all : plugin.boxingArena){
				all.sendMessage(PluginMeta.prefix + p.getDisplayName() + " �ahat die Box-Arena verlassen!");
			}
			
			if(plugin.boxingArena.contains(p)){
				plugin.boxingArena.remove(p);
			}
			
			p.setCanPickupItems(true);
		}
		
		if(e.getRegion().getId().equalsIgnoreCase("Trampolin")){
			plugin.schwatteWolle.remove(p);
		}
		
		if(e.getRegion().getId().equalsIgnoreCase("RaceBow")){
			plugin.raceBow.remove(p);
		}
		
		if(e.getRegion().getId().equalsIgnoreCase("BlockJump")){
			for(Player all : plugin.blockJump){
				all.sendMessage(PluginMeta.prefix + p.getDisplayName() + " �ahat den BlockJump verlassen!");
			}
			
			plugin.blockJump.remove(p);
		}
	}
	
	@EventHandler
	public void onPickUp(PlayerPickupItemEvent e){
		Player p = e.getPlayer();
		
		if(plugin.boxingArena.contains(p)){
			e.setCancelled(true);
		} else {
			e.setCancelled(false);
		}
	}
	
}