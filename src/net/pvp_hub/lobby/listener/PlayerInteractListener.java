package net.pvp_hub.lobby.listener;

import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.lobby.PvPHubLobby;
import net.pvp_hub.lobby.api.ItemUtilities;
import net.pvp_hub.lobby.api.Locations;
import net.pvp_hub.lobby.api.PluginMeta;
import net.pvp_hub.lobby.inv.AdminTools;
import net.pvp_hub.lobby.inv.EmeraldTP;
import net.pvp_hub.lobby.inv.Juke;
import net.pvp_hub.lobby.inv.LS_Main;
import net.pvp_hub.lobby.inv.SHOP_Herthi;
import net.pvp_hub.lobby.inv.SHOP_Rex;
import net.pvp_hub.lobby.inv.SHOP_Vips;
import net.pvp_hub.lobby.inv.SHOP_Zeryther;
import net.pvp_hub.lobby.inv.Settings;

import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class PlayerInteractListener implements Listener {

	private PvPHubLobby plugin;
	public PlayerInteractListener(PvPHubLobby plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e){
		Player p = e.getPlayer();
		
		try {
			ItemStack cock = ItemUtilities.namedItem(Material.COOKIE, "�b�lOmas Kekse", new String[]{"�7Rarit�t: �9[NORMAL]", "�7Kosten:", "  �e20 �aCoins"});
			if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK){
				if(p.getItemInHand().getType().equals(Material.EMERALD)){
					EmeraldTP.openFor(p);
				} else if(p.getItemInHand().getType().equals(Material.MAGMA_CREAM)){
					p.teleport(Locations.spawn);
					p.sendMessage(PluginMeta.prefix + "�aDu wurdest zum �cSpawn�r �ateleportiert.");
				} else if(p.getItemInHand().getType().equals(Material.BOOK)){
					if(PlayerUtilities.getRank(p.getName()) >= 9){
						AdminTools.openFor(p, "mainMenu");
					}
				} else if(p.getItemInHand().equals(cock)){
					p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 2));
					p.getItemInHand().setType(Material.AIR);
					p.sendMessage(PluginMeta.prefix + "�6Du hast einen Speed-Boost erhalten.");
					p.sendMessage(PluginMeta.prefix + "�6Dieser verschwindet sobald du die Lobby verl�sst.");
				} else if(p.getItemInHand().getType().equals(Material.QUARTZ)){
					Settings.openFor(p);
				} else if(p.getItemInHand().getType().equals(Material.NETHER_STAR)){
					plugin.useHidingItem(p);
				} else if(e.getClickedBlock().getType().equals(Material.ENCHANTMENT_TABLE)){
					e.setCancelled(true);
					LS_Main.openFor(p);
				} else if(e.getClickedBlock().getType().equals(Material.JUKEBOX)){
					e.setCancelled(true);
					Juke.openFor(p);
				} else if(plugin.disallowedBlocks.contains(e.getClickedBlock().getType())) {
					if(!p.isOp()){
						e.setCancelled(true);
					}
				}
			} 
		} catch (Exception e1){ }
	}
	
	@EventHandler
	public void onTpSign(PlayerInteractEvent e){
		final Player p = e.getPlayer();
		
		if(e.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
			if(e.getClickedBlock().getType() == Material.SIGN || e.getClickedBlock().getType() == Material.SIGN_POST || e.getClickedBlock().getType() == Material.WALL_SIGN){
				Sign sign = (Sign) e.getClickedBlock().getState();
				
				String line1 = sign.getLine(0);
				String line2 = sign.getLine(1);
				String line3 = sign.getLine(2);
				String line4 = sign.getLine(3);
				
				if(line2.equals("�4Block�eJump") &&
						   line3.equals("�aBelohnung")){
							try {
								PlayerUtilities.addCoins(p, 5);
								p.teleport(Locations.blockJump);
							} catch (Exception e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
				else if(line2.equals("�4Block�eJump") &&
						   line3.equals("�eBelohnung")){
							try {
								PlayerUtilities.addCoins(p, 10);
								p.teleport(Locations.blockJump);
							} catch (Exception e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
				else if(line2.equals("�4Block�eJump") &&
						   line3.equals("�9Belohnung")){
							try {
								PlayerUtilities.addCoins(p, 15);
								p.teleport(Locations.blockJump);
							} catch (Exception e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
				else if(line2.equals("�4Block�eJump") &&
						   line3.equals("�4Belohnung")){
							try {
								PlayerUtilities.addCoins(p, 20);
								p.teleport(Locations.blockJump);
							} catch (Exception e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
				}
			}
		}
	}
	
	@EventHandler
	public void onShopSign(PlayerInteractEvent e){
		Player p = e.getPlayer();
		if(e.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
			if(e.getClickedBlock().getType() == Material.SIGN || e.getClickedBlock().getType() == Material.SIGN_POST || e.getClickedBlock().getType() == Material.WALL_SIGN){
				Sign sign = (Sign) e.getClickedBlock().getState();
				
				String line1 = sign.getLine(0);
				String line2 = sign.getLine(1);
				String line3 = sign.getLine(2);
				String line4 = sign.getLine(3);
				
				if(line2.equals("�1[SHOP]")){
					if(line3.equals("Zeryther")){
						SHOP_Zeryther.openFor(p);
					}
					
					if(line3.equals("Materia__")){
						//SHOP_Zeryther.openFor(p);
					}
					
					if(line3.equals("Vips")){
						SHOP_Vips.openFor(p);
					}
					
					if(line3.equals("rex2go")){
						SHOP_Rex.openFor(p);
					}
					
					if(line3.equals("herthaner2_0")){
						SHOP_Herthi.openFor(p);
					}
				}
			}
		}
	}
}

