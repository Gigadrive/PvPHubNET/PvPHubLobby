package net.pvp_hub.lobby.listener;

import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.core.api.PluginMeta;
import net.pvp_hub.lobby.PvPHubLobby;
import net.pvp_hub.lobby.api.Locations;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

public class PlayerDeathListener implements Listener {

	private PvPHubLobby plugin;
	public PlayerDeathListener(PvPHubLobby plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onDeath(PlayerDeathEvent e){
		e.setDeathMessage(null);
		if(plugin.boxingArena.contains(e.getEntity())){
			if(e.getEntity().getKiller() == null){
				Bukkit.broadcastMessage(PluginMeta.prefix + e.getEntity().getDisplayName() + " �6ist gestorben.");
			} else {
				Bukkit.broadcastMessage(PluginMeta.prefix + e.getEntity().getDisplayName() + " �6wurde von " + e.getEntity().getKiller().getDisplayName() + " �6get�tet.");
				try {
					PlayerUtilities.addCoins(e.getEntity().getKiller(), 2);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
	}
	
	@EventHandler
	public void onRespawn(PlayerRespawnEvent e){
		e.getPlayer().teleport(Locations.spawn);
	}
	
}
