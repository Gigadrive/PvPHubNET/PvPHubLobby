package net.pvp_hub.lobby.listener;

import net.pvp_hub.lobby.PvPHubLobby;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class PlayerDamageListener implements Listener {

	private PvPHubLobby plugin;
	public PlayerDamageListener(PvPHubLobby plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onJoin(EntityDamageEvent e){
		if(e.getEntity() instanceof Player){
			if(plugin.boxingArena.contains((Player)e.getEntity())){
				e.setCancelled(false);
			} else {
				e.setCancelled(true);
			}
		}
	}
	
}
