package net.pvp_hub.lobby.listener;

import net.pvp_hub.core.api.PluginMeta;
import net.pvp_hub.lobby.PvPHubLobby;
import net.pvp_hub.lobby.api.Locations;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.material.Wool;

public class PlayerMoveListener implements Listener {
	
	private PvPHubLobby plugin;
	public PlayerMoveListener(PvPHubLobby plugin){
		this.plugin = plugin;
	}

	@EventHandler
	public void onMove(PlayerMoveEvent e){
		Player p = e.getPlayer();
		double x = p.getLocation().getX();
		double y = p.getLocation().getY();
		double z = p.getLocation().getZ();
		
		if(plugin.schwatteWolle.contains(p)){
			if(!p.isSneaking()) {
				Location bu = new Location(Bukkit.getWorld("world"), x, y - 1, z);
				Location bubu = new Location(Bukkit.getWorld("world"), x, y - 2, z);
				if(bu.getBlock().getType() == Material.WOOL) {
					Wool wool = (Wool) bu.getBlock().getState().getData();
					if(wool.getColor() == DyeColor.BLACK) {
						p.setVelocity(p.getVelocity().setY(5 / 9.0D));
						//p.lastTrampUse = System.currentTimeMillis();
					}
				}
				else if(bubu.getBlock().getType() == Material.WOOL) {
					Wool wool = (Wool) bubu.getBlock().getState().getData();
					if(wool.getColor() == DyeColor.BLACK) {
						p.setVelocity(p.getVelocity().setY(5 / 9.0D));
						//p.lastTrampUse = System.currentTimeMillis();
					}
				}
			}
		}
		
		if(!p.isSneaking()) {
			Location bu = new Location(Bukkit.getWorld("world"), x, y - 1, z);
			Location bubu = new Location(Bukkit.getWorld("world"), x, y - 2, z);
			if(bu.getBlock().getType() == Material.SPONGE) {
				p.setVelocity(p.getVelocity().setY(17 / 9.0D));
			}
			else if(bubu.getBlock().getType() == Material.SPONGE) {
				p.setVelocity(p.getVelocity().setY(17 / 9.0D));
			}
			else if(bu.getBlock().getType() == Material.RAILS){
				if(!p.isOp()){
					p.teleport(Locations.spawn);
					p.sendMessage(PluginMeta.prefix + "�cDu darfst nicht auf den Schienen stehen!");
				}
			}
			else if(bubu.getBlock().getType() == Material.RAILS){
				if(!p.isOp()){
					p.teleport(Locations.spawn);
					p.sendMessage(PluginMeta.prefix + "�cDu darfst nicht auf den Schienen stehen!");
				}
			}
			else if(bu.getBlock().getType() == Material.POWERED_RAIL){
				if(!p.isOp()){
					p.teleport(Locations.spawn);
					p.sendMessage(PluginMeta.prefix + "�cDu darfst nicht auf den Schienen stehen!");
				}
			}
			else if(bubu.getBlock().getType() == Material.POWERED_RAIL){
				if(!p.isOp()){
					p.teleport(Locations.spawn);
					p.sendMessage(PluginMeta.prefix + "�cDu darfst nicht auf den Schienen stehen!");
				}
			}
		}
	}
	
}
