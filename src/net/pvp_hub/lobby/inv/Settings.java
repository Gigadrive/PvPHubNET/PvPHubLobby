package net.pvp_hub.lobby.inv;

import java.util.ArrayList;

import net.pvp_hub.core.PvPHubCore;
import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.lobby.api.ItemUtilities;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

public class Settings {

	private static Inventory kl;
	
	public static void openFor(Player p){
	    initList(p);
	    p.openInventory(kl);
	}
	
	private static String nickItemName(Player p){
		String r = "";
		
		try {
			if(PlayerUtilities.hasAutoNick(p) == 1){
				r = "�5�lAuto-Nick �r�8| �aAktiviert";
			} else {
				r = "�5�lAuto-Nick �r�8| �cDeaktiviert";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return r;
	}
	
	private static String statsItemName(Player p){
		String r = "";
		
		try {
			if(PlayerUtilities.hasShowStats(p) == 1){
				r = "�a�lZeige Stats �r�8| �aAktiviert";
			} else {
				r = "�a�lZeige Stats �r�8| �cDeaktiviert";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return r;
	}
	
	private static String teamSpawnItemName(Player p){
		String r = "";
		
		try {
			if(PlayerUtilities.hasTeamSpawn(p) == 1){
				r = "�b�lTeam Spawn �r�8| �aAktiviert";
			} else {
				r = "�b�lTeam Spawn �r�8| �cDeaktiviert";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return r;
	}
	
	private static String friendsItemName(Player p){
		String r = "";
		
		try {
			if(PlayerUtilities.hasFriends(p) == 1){
				r = "�6�lFreundschaftsanfragen �r�8| �aAktiviert";
			} else {
				r = "�6�lFreundschaftsanfragen �r�8| �cDeaktiviert";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return r;
	}
	
	private static String showServerLocation(Player p){
		String r = "";
		
		try {
			if(PlayerUtilities.hasServerLoc(p) == 1){
				r = "�2�lZeige Ort �r�8| �aAktiviert";
			} else {
				r = "�2�lZeige Ort �r�8| �cDeaktiviert";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return r;
	}
	
	public static void initList(Player p){
		ItemStack fr = null;
		fr = new ItemStack(Material.SKULL_ITEM);
		fr.setDurability((short) 3);
		ItemMeta frM = fr.getItemMeta();
		frM.setDisplayName(Settings.friendsItemName(p));
		ArrayList<String> frLo = new ArrayList<String>();
		frLo.add("�7M�chtest du Freundschaftsanfragen erhalten k�nnen?");
		frLo.add("�cTeam-Mitglieder k�nnen dir immer Freundschaftsanfragen schicken.");
		frM.setLore(frLo);
		fr.setItemMeta(frM);
		
			kl = Bukkit.createInventory(null, 27, "�9Einstellungen");
			kl.setItem(0, ItemUtilities.namedItem(Material.STAINED_GLASS_PANE, " ", new String[]{""}));
			kl.setItem(1, ItemUtilities.namedItem(Material.STAINED_GLASS_PANE, " ", new String[]{""}));
			kl.setItem(2, ItemUtilities.namedItem(Material.STAINED_GLASS_PANE, " ", new String[]{""}));
			kl.setItem(3, ItemUtilities.namedItem(Material.STAINED_GLASS_PANE, " ", new String[]{""}));
			kl.setItem(4, ItemUtilities.namedItem(Material.STAINED_GLASS_PANE, " ", new String[]{""}));
			kl.setItem(5, ItemUtilities.namedItem(Material.STAINED_GLASS_PANE, " ", new String[]{""}));
			kl.setItem(6, ItemUtilities.namedItem(Material.STAINED_GLASS_PANE, " ", new String[]{""}));
			kl.setItem(7, ItemUtilities.namedItem(Material.STAINED_GLASS_PANE, " ", new String[]{""}));
			kl.setItem(8, ItemUtilities.namedItem(Material.STAINED_GLASS_PANE, " ", new String[]{""}));
			
			kl.setItem(9, ItemUtilities.namedItem(Material.NAME_TAG, Settings.nickItemName(p), new String[]{"�7M�chtest du, wenn du die Lobby betrittst genickt werden?", "�cDies ist ein Feature f�r VIPs und h�her!"}));
			kl.setItem(10, ItemUtilities.namedItem(Material.BOOK, Settings.statsItemName(p), new String[]{"�7M�chtest du, dass andere deine Stats mit /stats einsehen k�nnen?"}));
			kl.setItem(11, fr);
			kl.setItem(12, ItemUtilities.namedItem(Material.GHAST_TEAR, Settings.showServerLocation(p), new String[]{"�7M�chtest du, dass andere deinen aktuellen", "�7Server auf der Website sehen k�nnen?"}));
			
			try {
				if(PlayerUtilities.getRank(p.getName()) >= 5){
					kl.setItem(17, ItemUtilities.namedItem(Material.REDSTONE, Settings.teamSpawnItemName(p), new String[]{"�7M�chtest du, beim Login aufs Luftschiff teleportiert werden?"}));
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			kl.setItem(18, ItemUtilities.namedItem(Material.STAINED_GLASS_PANE, " ", new String[]{""}));
			kl.setItem(19, ItemUtilities.namedItem(Material.STAINED_GLASS_PANE, " ", new String[]{""}));
			kl.setItem(20, ItemUtilities.namedItem(Material.STAINED_GLASS_PANE, " ", new String[]{""}));
			kl.setItem(21, ItemUtilities.namedItem(Material.STAINED_GLASS_PANE, " ", new String[]{""}));
			kl.setItem(22, ItemUtilities.namedItem(Material.STAINED_GLASS_PANE, " ", new String[]{""}));
			kl.setItem(23, ItemUtilities.namedItem(Material.STAINED_GLASS_PANE, " ", new String[]{""}));
			kl.setItem(24, ItemUtilities.namedItem(Material.STAINED_GLASS_PANE, " ", new String[]{""}));
			kl.setItem(25, ItemUtilities.namedItem(Material.STAINED_GLASS_PANE, " ", new String[]{""}));
			kl.setItem(26, ItemUtilities.namedItem(Material.STAINED_GLASS_PANE, " ", new String[]{""}));
		}
	}
	

