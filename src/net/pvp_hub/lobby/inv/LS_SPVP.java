package net.pvp_hub.lobby.inv;

import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.lobby.api.ItemUtilities;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class LS_SPVP {

	private static Inventory kl;
	
	public static void openFor(Player p){
		initList(p);
		p.openInventory(kl);
	}
	
	public static void initList(Player p){
		try {
			kl = Bukkit.createInventory(null, 27, "�eSoupPvP-Shop");
			kl.setItem(0, new ItemStack(Material.STAINED_GLASS_PANE));
			kl.setItem(1, new ItemStack(Material.STAINED_GLASS_PANE));
			kl.setItem(2, new ItemStack(Material.STAINED_GLASS_PANE));
			kl.setItem(3, new ItemStack(Material.STAINED_GLASS_PANE));
			kl.setItem(4, new ItemStack(Material.STAINED_GLASS_PANE));
			kl.setItem(5, new ItemStack(Material.STAINED_GLASS_PANE));
			kl.setItem(6, new ItemStack(Material.STAINED_GLASS_PANE));
			kl.setItem(7, new ItemStack(Material.STAINED_GLASS_PANE));
			kl.setItem(8, new ItemStack(Material.STAINED_GLASS_PANE));
			
			if(PlayerUtilities.hasBoughtShopItem(p, 17)){
				kl.setItem(9, ItemUtilities.namedItem(Material.CHEST, "�c�lAUSVERKAUFT!", new String[] {"�7Du hast dieses Item bereits gekauft."}));
			} else {
				kl.setItem(9, ItemUtilities.namedItem(Material.STICK, "�5�lWizard", new String[] {"�7Kosten:", "�e  " + PlayerUtilities.getCoinsForItem(17) + " �aCoins", "�e  " + PlayerUtilities.getChipsForItem(17) + " �6Chips"}));
			}
			
			if(PlayerUtilities.hasBoughtShopItem(p, 18)){
				kl.setItem(10, ItemUtilities.namedItem(Material.CHEST, "�c�lAUSVERKAUFT!", new String[] {"�7Du hast dieses Item bereits gekauft."}));
			} else {
				kl.setItem(10, ItemUtilities.namedItem(Material.DIAMOND_SWORD, "�3�lSurvivor", new String[] {"�7Kosten:", "�e  " + PlayerUtilities.getCoinsForItem(18) + " �aCoins", "�e  " + PlayerUtilities.getChipsForItem(18) + " �6Chips"}));
			}
			
			if(PlayerUtilities.hasBoughtShopItem(p, 19)){
				kl.setItem(11, ItemUtilities.namedItem(Material.CHEST, "�c�lAUSVERKAUFT!", new String[] {"�7Du hast dieses Item bereits gekauft."}));
			} else {
				kl.setItem(11, ItemUtilities.namedItem(Material.DIAMOND_CHESTPLATE, "�b�lHeavy", new String[] {"�7Kosten:", "�e  " + PlayerUtilities.getCoinsForItem(19) + " �aCoins", "�e  " + PlayerUtilities.getChipsForItem(19) + " �6Chips"}));
			}
			
			kl.setItem(18, new ItemStack(Material.STAINED_GLASS_PANE));
			kl.setItem(19, new ItemStack(Material.STAINED_GLASS_PANE));
			kl.setItem(20, new ItemStack(Material.STAINED_GLASS_PANE));
			kl.setItem(21, new ItemStack(Material.STAINED_GLASS_PANE));
			kl.setItem(22, new ItemStack(Material.STAINED_GLASS_PANE));
			kl.setItem(23, new ItemStack(Material.STAINED_GLASS_PANE));
			kl.setItem(24, new ItemStack(Material.STAINED_GLASS_PANE));
			kl.setItem(25, new ItemStack(Material.STAINED_GLASS_PANE));
			kl.setItem(26, new ItemStack(Material.STAINED_GLASS_PANE));
		} catch (Exception e){}
	}
	
}
