package net.pvp_hub.lobby.inv;

import net.pvp_hub.lobby.api.ItemUtilities;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class LS_Clothes {

private static Inventory kl;
	
	public static void openFor(Player p){
		initList();
	    p.openInventory(kl);
	}
	
	public static void initList(){
		ItemStack ph = new ItemStack(Material.STAINED_GLASS_PANE);
		ItemMeta phM = ph.getItemMeta();
		phM.setDisplayName("");
		ph.setItemMeta(phM);
		ph.setDurability((short) 0);
		
		ItemStack b = new ItemStack(Material.DIAMOND);
		ItemMeta bM = b.getItemMeta();
		bM.setDisplayName("�b�lZur�ck");
		b.setItemMeta(bM);
		
		ItemStack rm = new ItemStack(Material.REDSTONE_BLOCK);
		ItemMeta rmM = rm.getItemMeta();
		rmM.setDisplayName("�c�lAusziehen");
		rm.setItemMeta(rmM);
		
		if(kl == null){
			kl = Bukkit.createInventory(null, 54, "Kleiderschrank");
			kl.setItem(0, ph);
			kl.setItem(1, ph);
			kl.setItem(2, ph);
			kl.setItem(3, ph);
			kl.setItem(4, ph);
			kl.setItem(5, ph);
			kl.setItem(6, ph);
			kl.setItem(7, ph);
			kl.setItem(8, ph);
			
			kl.setItem(9, ItemUtilities.namedItem(Material.LEATHER_HELMET, "�8Lederhelm", new String[] { "" }));
			kl.setItem(10, ItemUtilities.namedItem(Material.IRON_HELMET, "�fEisenhelm", new String[] { "" }));
			kl.setItem(11, ItemUtilities.namedItem(Material.CHAINMAIL_HELMET, "�7Kettenhelm", new String[] { "" }));
			kl.setItem(12, ItemUtilities.namedItem(Material.DIAMOND_HELMET, "�bDiamanthelm", new String[] { "" }));
			kl.setItem(13, ItemUtilities.namedItem(Material.GOLD_HELMET, "�6Goldhelm", new String[] { "" }));
			
			kl.setItem(18, ItemUtilities.namedItem(Material.LEATHER_CHESTPLATE, "�8Lederbrustplatte", new String[] { "" }));
			kl.setItem(19, ItemUtilities.namedItem(Material.IRON_CHESTPLATE, "�fEisenbrustplatte", new String[] { "" }));
			kl.setItem(20, ItemUtilities.namedItem(Material.CHAINMAIL_CHESTPLATE, "�7Kettenbrustplatte", new String[] { "" }));
			kl.setItem(21, ItemUtilities.namedItem(Material.DIAMOND_CHESTPLATE, "�bDiamantbrustplatte", new String[] { "" }));
			kl.setItem(22, ItemUtilities.namedItem(Material.GOLD_CHESTPLATE, "�6Goldbrustplatte", new String[] { "" }));
			
			kl.setItem(27, ItemUtilities.namedItem(Material.LEATHER_LEGGINGS, "�8Lederhose", new String[] { "" }));
			kl.setItem(28, ItemUtilities.namedItem(Material.IRON_LEGGINGS, "�fEisenhose", new String[] { "" }));
			kl.setItem(29, ItemUtilities.namedItem(Material.CHAINMAIL_LEGGINGS, "�7Kettenhose", new String[] { "" }));
			kl.setItem(30, ItemUtilities.namedItem(Material.DIAMOND_LEGGINGS, "�bDiamanthose", new String[] { "" }));
			kl.setItem(31, ItemUtilities.namedItem(Material.GOLD_LEGGINGS, "�6Goldhose", new String[] { "" }));
			
			kl.setItem(36, ItemUtilities.namedItem(Material.LEATHER_BOOTS, "�8Lederbrustplatte", new String[] { "" }));
			kl.setItem(37, ItemUtilities.namedItem(Material.IRON_BOOTS, "�7Eisenschuhe", new String[] { "" }));
			kl.setItem(38, ItemUtilities.namedItem(Material.CHAINMAIL_BOOTS, "�7Kettenschuhe", new String[] { "" }));
			kl.setItem(39, ItemUtilities.namedItem(Material.DIAMOND_BOOTS, "�bDiamantschuhe", new String[] { "" }));
			kl.setItem(40, ItemUtilities.namedItem(Material.GOLD_BOOTS, "�6Goldschuhe", new String[] { "" }));
			
			kl.setItem(45, ph);
			kl.setItem(46, ph);
			kl.setItem(47, ph);
			kl.setItem(48, b);
			kl.setItem(49, ph);
			kl.setItem(50, rm);
			kl.setItem(51, ph);
			kl.setItem(52, ph);
			kl.setItem(53, ph);
		}
	}
	
}
