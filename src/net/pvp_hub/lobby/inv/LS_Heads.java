package net.pvp_hub.lobby.inv;

import net.pvp_hub.lobby.api.ItemUtilities;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class LS_Heads {

	private static Inventory kl;
	
	public static void openFor(Player p){
		initList();
		p.openInventory(kl);
	}
	
	private static void initList(){
		ItemStack zery = new ItemStack(Material.SKULL_ITEM);
		zery.setDurability((short)3);
		ItemMeta zeryM = zery.getItemMeta();
		zeryM.setDisplayName("�7Kopf von �eZeryther");
		zery.setItemMeta(zeryM);
		
		ItemStack vips = new ItemStack(Material.SKULL_ITEM);
		vips.setDurability((short)3);
		ItemMeta vipsM = vips.getItemMeta();
		vipsM.setDisplayName("�7Kopf von �eVipsNiceMinePlay");
		vips.setItemMeta(vipsM);
		
		ItemStack dub = new ItemStack(Material.SKULL_ITEM);
		dub.setDurability((short)3);
		ItemMeta dubM = dub.getItemMeta();
		dubM.setDisplayName("�7Kopf von �ecrafter14360");
		dub.setItemMeta(dubM);
		
		ItemStack chick = new ItemStack(Material.SKULL_ITEM);
		chick.setDurability((short)3);
		ItemMeta chickM = chick.getItemMeta();
		chickM.setDisplayName("�7Kopf von einem �eH�hnchen");
		chick.setItemMeta(chickM);
		
		ItemStack pig = new ItemStack(Material.SKULL_ITEM);
		pig.setDurability((short)3);
		ItemMeta pigM = pig.getItemMeta();
		pigM.setDisplayName("�7Kopf von einem �eSchwein");
		pig.setItemMeta(pigM);
		
		if(kl == null){
			kl = Bukkit.createInventory(null, 9, "�6H�te");
			kl.setItem(0, zery);
			kl.setItem(1, vips);
			kl.setItem(2, dub);
			kl.setItem(3, chick);
			kl.setItem(4, pig);
			
			kl.setItem(8, ItemUtilities.namedItem(Material.REDSTONE_BLOCK, "�c�lHut absetzen", new String[]{""}));
		}
	}
	
}
