package net.pvp_hub.lobby.inv;

import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.lobby.api.ItemUtilities;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class BuyItemMenu {

	private static Inventory kl;
	
	public static void openFor(Player p, String w){
		initList(p, w);
		p.openInventory(kl);
	}
	
	public static void initList(Player p, String w){
		try {
			if(w == "�3�lSurvivor"){
				kl = Bukkit.createInventory(null, 9, "�3�lSurvivor");
				kl.setItem(1, ItemUtilities.namedItem(Material.GOLD_NUGGET, "�a�lMit Coins kaufen", new String[] {"�7Kosten: �e" + PlayerUtilities.getCoinsForItem(18) + " �aCoins"}));
				kl.setItem(4, ItemUtilities.namedItem(Material.NAME_TAG, "�6�lMit Chips kaufen", new String[] {"�7Kosten: �e" + PlayerUtilities.getChipsForItem(18) + " �6Coins"}));
				kl.setItem(8, ItemUtilities.namedItem(Material.REDSTONE, "�c�lAbbrechen", new String[] {""}));
			} else if(w == "�b�lHeavy"){
				kl = Bukkit.createInventory(null, 9, "�b�lHeavy");
				kl.setItem(1, ItemUtilities.namedItem(Material.GOLD_NUGGET, "�a�lMit Coins kaufen", new String[] {"�7Kosten: �e" + PlayerUtilities.getCoinsForItem(19) + " �aCoins"}));
				kl.setItem(4, ItemUtilities.namedItem(Material.NAME_TAG, "�6�lMit Chips kaufen", new String[] {"�7Kosten: �e" + PlayerUtilities.getChipsForItem(19) + " �6Coins"}));
				kl.setItem(8, ItemUtilities.namedItem(Material.REDSTONE, "�c�lAbbrechen", new String[] {""}));
			} else if(w == "�5�lWizard"){
				kl = Bukkit.createInventory(null, 9, "�5�lWizard");
				kl.setItem(1, ItemUtilities.namedItem(Material.GOLD_NUGGET, "�a�lMit Coins kaufen", new String[] {"�7Kosten: �e" + PlayerUtilities.getCoinsForItem(17) + " �aCoins"}));
				kl.setItem(4, ItemUtilities.namedItem(Material.NAME_TAG, "�6�lMit Chips kaufen", new String[] {"�7Kosten: �e" + PlayerUtilities.getChipsForItem(17) + " �6Coins"}));
				kl.setItem(8, ItemUtilities.namedItem(Material.REDSTONE, "�c�lAbbrechen", new String[] {""}));
			} else if(w == "�7�lLederr�stung �a(Starterkit)"){
				kl = Bukkit.createInventory(null, 9, "�7�lLederr�stung �a(Starterkit)");
				kl.setItem(1, ItemUtilities.namedItem(Material.GOLD_NUGGET, "�a�lMit Coins kaufen", new String[] {"�7Kosten: �e" + PlayerUtilities.getCoinsForItem(20) + " �aCoins"}));
				kl.setItem(4, ItemUtilities.namedItem(Material.NAME_TAG, "�6�lMit Chips kaufen", new String[] {"�7Kosten: �e" + PlayerUtilities.getChipsForItem(20) + " �6Coins"}));
				kl.setItem(8, ItemUtilities.namedItem(Material.REDSTONE, "�c�lAbbrechen", new String[] {""}));
			} else {
				return;
			}
		} catch (Exception e){}
	}
	
}
