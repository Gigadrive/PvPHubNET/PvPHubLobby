package net.pvp_hub.lobby.inv;

import net.pvp_hub.lobby.api.ItemUtilities;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class LS_Main {

private static Inventory kl;
	
	public static void openFor(Player p){
		initList();
	    p.openInventory(kl);
	}
	
	public static void initList(){
		ItemStack ph = new ItemStack(Material.STAINED_GLASS_PANE);
		ItemMeta phM = ph.getItemMeta();
		phM.setDisplayName("");
		ph.setItemMeta(phM);
		ph.setDurability((short) 0);
		
		ItemStack cr = new ItemStack(Material.MONSTER_EGG);
		ItemMeta crM = cr.getItemMeta();
		crM.setDisplayName("�2Pet Shop");
		cr.setItemMeta(crM);
		cr.setDurability((short) 50);
		
		ItemStack hu = new ItemStack(Material.SKULL_ITEM);
		ItemMeta huM = hu.getItemMeta();
		huM.setDisplayName("�6H�te");
		hu.setItemMeta(huM);
		hu.setDurability((short) 3);
		
		if(kl == null){
			kl = Bukkit.createInventory(null, 18, "Lobby-Shop");
			kl.setItem(0, ph);
			kl.setItem(1, ItemUtilities.namedItem(Material.MUSHROOM_SOUP, "�eSoupPvP Shop", new String[] {""}));
			kl.setItem(2, ph);
			kl.setItem(3, ph);
			kl.setItem(4, ItemUtilities.namedItem(Material.DIAMOND_CHESTPLATE, "�3DeathMatch Shop", new String[]{""}));
			kl.setItem(5, ph);
			kl.setItem(6, ph);
			kl.setItem(7, ItemUtilities.namedItem(Material.ROTTEN_FLESH, "�2InfectionWars Shop", new String[]{""}));
			kl.setItem(8, ph);
			
			kl.setItem(9, ph);
			kl.setItem(10, cr);
			kl.setItem(11, ph);
			kl.setItem(12, ph);
			kl.setItem(13, ItemUtilities.namedItem(Material.LEATHER_CHESTPLATE, "�5Kleiderschrank", new String[] {""}));
			kl.setItem(14, ph);
			kl.setItem(15, ph);
			kl.setItem(16, hu);
			kl.setItem(17, ph);
		}
	}
	
}
