package net.pvp_hub.lobby.inv;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Juke {

private static Inventory kl;
	
	public static void openFor(Player p){
		initList();
	    p.openInventory(kl);
	}
	
	public static void initList(){
		ItemStack thirteen = new ItemStack(Material.GOLD_RECORD);
		thirteen.setDurability((short)2256);
		ItemMeta thM = thirteen.getItemMeta();
		thM.setDisplayName("�6�l13");
		thirteen.setItemMeta(thM);
		
		ItemStack cat = new ItemStack(Material.GREEN_RECORD);
		cat.setDurability((short)2257);
		ItemMeta catM = cat.getItemMeta();
		catM.setDisplayName("�a�lCat");
		cat.setItemMeta(catM);
		
		ItemStack blocks = new ItemStack(Material.RECORD_3);
		blocks.setDurability((short)2258);
		ItemMeta bloM = blocks.getItemMeta();
		bloM.setDisplayName("�c�lBlocks");
		blocks.setItemMeta(bloM);
		
		ItemStack chirp = new ItemStack(Material.RECORD_4);
		chirp.setDurability((short)2259);
		ItemMeta chiM = chirp.getItemMeta();
		chiM.setDisplayName("�4�lChirp");
		chirp.setItemMeta(chiM);
		
		ItemStack far = new ItemStack(Material.RECORD_5);
		far.setDurability((short)2260);
		ItemMeta farM = far.getItemMeta();
		farM.setDisplayName("�a�lFar");
		far.setItemMeta(farM);
		
		ItemStack mall = new ItemStack(Material.RECORD_6);
		mall.setDurability((short)2261);
		ItemMeta malM = mall.getItemMeta();
		malM.setDisplayName("�5�lMall");
		mall.setItemMeta(malM);
		
		ItemStack mellohi = new ItemStack(Material.RECORD_7);
		mellohi.setDurability((short)2262);
		ItemMeta melM = mellohi.getItemMeta();
		melM.setDisplayName("�d�lMellohi");
		mellohi.setItemMeta(melM);
		
		ItemStack stal = new ItemStack(Material.RECORD_8);
		stal.setDurability((short)2263);
		ItemMeta stalM = stal.getItemMeta();
		stalM.setDisplayName("�8�lStal");
		stal.setItemMeta(stalM);
		
		ItemStack strad = new ItemStack(Material.RECORD_9);
		strad.setDurability((short)2264);
		ItemMeta straM = strad.getItemMeta();
		straM.setDisplayName("�f�lStrad");
		strad.setItemMeta(straM);
		
		ItemStack ward = new ItemStack(Material.RECORD_10);
		ward.setDurability((short)2265);
		ItemMeta warM = ward.getItemMeta();
		warM.setDisplayName("�2�lWard");
		ward.setItemMeta(warM);
		
		ItemStack eleven = new ItemStack(Material.RECORD_11);
		eleven.setDurability((short)2266);
		ItemMeta elM = eleven.getItemMeta();
		elM.setDisplayName("�8�l11");
		eleven.setItemMeta(elM);
		
		ItemStack wait = new ItemStack(Material.RECORD_12);
		wait.setDurability((short)2267);
		ItemMeta waiM = wait.getItemMeta();
		waiM.setDisplayName("�b�lWait");
		wait.setItemMeta(waiM);
		
		ItemStack cancel = new ItemStack(Material.REDSTONE_BLOCK);
		ItemMeta canM = cancel.getItemMeta();
		canM.setDisplayName("�c�lMusik stoppen");
		cancel.setItemMeta(canM);
		
		kl = Bukkit.createInventory(null, 18, "�5JukeBox");
		kl.setItem(0, thirteen);
		kl.setItem(1, cat);
		kl.setItem(2, blocks);
		kl.setItem(3, chirp);
		kl.setItem(4, far);
		kl.setItem(5, mall);
		kl.setItem(6, mellohi);
		kl.setItem(7, stal);
		kl.setItem(8, strad);
		kl.setItem(9, ward);
		kl.setItem(10, eleven);
		kl.setItem(11, wait);
	}
	
}
