package net.pvp_hub.lobby.inv;

import net.pvp_hub.lobby.api.ItemUtilities;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class SHOP_Zeryther {

	private static Inventory kl;
	public static void openFor(Player p){
		initList();
		p.openInventory(kl);
	}
	
	public static void initList(){
		if(kl == null){
			kl = Bukkit.createInventory(null, 9, "Zeryther");
			kl.setItem(0, ItemUtilities.namedItem(Material.APPLE, "�b�lNewtons Apfel", new String[]{"�7Rarit�t: �3[UNNORMAL]", "�7Beschreibung: �fEin Apfel von Zeryther.", "�7Er soll wohl der Apfel sein, der Newton", "�7auf den Kopf gefallen ist.", "�7Kosten:", "  �e80 �aCoins"}));
			kl.setItem(1, ItemUtilities.namedItem(Material.COOKED_BEEF, "�b�lT-Bone Steak", new String[]{"�7Rarit�t: �9[NORMAL]", "�7Beschreibung: �fEin herzhaft gebratenes Steak aus dem", "�7Shop von Zeryther.", "�7Kosten:", "  �e95 �aCoins"}));
			kl.setItem(2, ItemUtilities.namedItem(Material.COOKIE, "�b�lOmas Kekse", new String[]{"�7Rarit�t: �9[NORMAL]", "�7Kosten:", "  �e20 �aCoins"}));
		}
	}
	
}
