package net.pvp_hub.lobby.inv;

import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.core.language.Language;
import net.pvp_hub.lobby.api.ItemUtilities;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class EmeraldTP {

private static Inventory kl;
	
	public static void openFor(Player p){
		initList(p);
	    p.openInventory(kl);
	}
	
	public static void initList(Player p){
		ItemStack placehold = new ItemStack(Material.STAINED_GLASS_PANE);
		ItemMeta placM = placehold.getItemMeta();
		placM.setDisplayName("");
		placehold.setItemMeta(placM);
		placehold.setDurability((short) 15);
		
		if(kl == null){
			try {
				kl = Bukkit.createInventory(null, 45, "            �2-=- Spielmodi -=-");
				kl.setItem(0, placehold);
				kl.setItem(1, placehold);
				kl.setItem(2, placehold);
				kl.setItem(3, placehold);
				kl.setItem(4, placehold);
				kl.setItem(5, placehold);
				kl.setItem(6, placehold);
				kl.setItem(7, placehold);
				kl.setItem(8, placehold);
				
				kl.setItem(9, placehold);
				kl.setItem(10, placehold);
				kl.setItem(11, placehold);
				kl.setItem(12, placehold);
				kl.setItem(13, ItemUtilities.namedItem(Material.MAGMA_CREAM, "�c�lSpawn", new String[]{"�7Teleportiere dich zum Spawn"}));
				kl.setItem(14, placehold);
				kl.setItem(15, placehold);
				kl.setItem(16, placehold);
				kl.setItem(17, placehold);
				
				kl.setItem(18, placehold);
				kl.setItem(19, placehold);
				kl.setItem(20, ItemUtilities.namedItem(Material.CHEST, "�4Survival Games", new String[]{Language.getTranslation(PlayerUtilities.getLanguage(p)).goToSurvivalGames()}));
				kl.setItem(21, ItemUtilities.namedItem(Material.ROTTEN_FLESH, "�2Infection Wars", new String[]{Language.getTranslation(PlayerUtilities.getLanguage(p)).goToInfWars()}));
				kl.setItem(22, ItemUtilities.namedItem(Material.DIAMOND_CHESTPLATE, "�3DeathMatch", new String[]{Language.getTranslation(PlayerUtilities.getLanguage(p)).goToDeathmatch()}));
				kl.setItem(23, ItemUtilities.namedItem(Material.MUSHROOM_SOUP, "�eSoupPvP", new String[]{"�7Zu den �eSoupPvP �7Servern."}));
				kl.setItem(24, ItemUtilities.namedItem(Material.FEATHER, "�4Block�eJump", new String[]{"�7Zu �4Block�eJump�7."}));
				kl.setItem(25, placehold);
				kl.setItem(26, placehold);
				
				kl.setItem(27, placehold);
				kl.setItem(28, placehold);
				kl.setItem(29, ItemUtilities.namedItem(Material.LEATHER_BOOTS, "�dRaceBow", new String[]{"�7Zu �dRaceBow�7."}));
				kl.setItem(30, ItemUtilities.namedItem(Material.BOW, "�aComing soon", new String[]{"�7Zu den �a??? �7Servern."}));
				kl.setItem(31, ItemUtilities.namedItem(Material.DIAMOND_PICKAXE, "�6Coming soon", new String[]{"�7Zu den �6??? �7Servern."}));
				kl.setItem(32, ItemUtilities.namedItem(Material.OBSIDIAN, "�5Coming soon", new String[]{"�7Zu den �5??? �7Servern."}));
				kl.setItem(33, ItemUtilities.namedItem(Material.BED, "�fComing soon", new String[]{"�7Zu den �f??? �7Servern."}));
				kl.setItem(34, placehold);
				kl.setItem(35, placehold);
				
				kl.setItem(36, placehold);
				kl.setItem(37, placehold);
				kl.setItem(38, placehold);
				kl.setItem(39, placehold);
				kl.setItem(40, placehold);
				kl.setItem(41, placehold);
				kl.setItem(42, placehold);
				kl.setItem(43, placehold);
				kl.setItem(44, placehold);
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
}
