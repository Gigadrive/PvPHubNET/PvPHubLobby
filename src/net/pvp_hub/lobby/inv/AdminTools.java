package net.pvp_hub.lobby.inv;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class AdminTools {

private static Inventory kl;
	
	public static void openFor(Player p, String w){
		initList(w);
	    p.openInventory(kl);
	}
	
	public static void initList(String w){
		if(w == "mainMenu"){
			ItemStack sh = new ItemStack(Material.REDSTONE_BLOCK);
			ItemMeta shM = sh.getItemMeta();
			shM.setDisplayName("�c�lLobby neuladen");
			sh.setItemMeta(shM);
			
			ItemStack gm = new ItemStack(Material.DIAMOND_PICKAXE);
			ItemMeta gmM = gm.getItemMeta();
			gmM.setDisplayName("�a�lGamemode �ndern");
			gm.setItemMeta(gmM);
			
			ItemStack ti = new ItemStack(Material.WATCH);
			ItemMeta tiM = ti.getItemMeta();
			tiM.setDisplayName("�e�lZeit �ndern");
			ti.setItemMeta(tiM);
			
			ItemStack br = new ItemStack(Material.NETHER_BRICK_ITEM);
			ItemMeta brM = br.getItemMeta();
			brM.setDisplayName("�5�lUnsichtbar machen");
			br.setItemMeta(brM);
			
			ItemStack sp = new ItemStack(Material.EMERALD);
			ItemMeta spM = sp.getItemMeta();
			spM.setDisplayName("�2�lSpielmodi deaktivieren");
			sp.setItemMeta(spM);
			
			ItemStack se = new ItemStack(Material.QUARTZ);
			ItemMeta seM = se.getItemMeta();
			seM.setDisplayName("�9�lEinstellungen deaktivieren");
			se.setItemMeta(seM);
			
			ItemStack hi = new ItemStack(Material.NETHER_STAR);
			ItemMeta hiM = hi.getItemMeta();
			hiM.setDisplayName("�6�lSpieler verstecken deaktivieren");
			hi.setItemMeta(hiM);
			
			ItemStack spa = new ItemStack(Material.MAGMA_CREAM);
			ItemMeta spaM = spa.getItemMeta();
			spaM.setDisplayName("�c�lSpawn-Item deaktivieren");
			spa.setItemMeta(spaM);
			
			kl = Bukkit.createInventory(null, 18, "�4Admin Tools");
			kl.setItem(1, sh);
			kl.setItem(3, gm);
			kl.setItem(5, ti);
			kl.setItem(7, br);
			kl.setItem(10, sp);
			kl.setItem(12, se);
			kl.setItem(14, hi);
			kl.setItem(16, spa);
			return;
		} else if(w == "gamemode"){
			ItemStack sh = new ItemStack(Material.IRON_BLOCK);
			ItemMeta shM = sh.getItemMeta();
			shM.setDisplayName("�7Survival");
			sh.setItemMeta(shM);
			
			ItemStack gm = new ItemStack(Material.GOLD_BLOCK);
			ItemMeta gmM = gm.getItemMeta();
			gmM.setDisplayName("�6Adventure");
			gm.setItemMeta(gmM);
			
			ItemStack ti = new ItemStack(Material.DIAMOND_BLOCK);
			ItemMeta tiM = ti.getItemMeta();
			tiM.setDisplayName("�bCreative");
			ti.setItemMeta(tiM);
			
			kl = Bukkit.createInventory(null, 9, "�4Gamemode �ndern");
			kl.setItem(1, sh);
			kl.setItem(4, gm);
			kl.setItem(7, ti);
			return;
		} else if(w == "time"){
			ItemStack sh = new ItemStack(Material.WATCH);
			ItemMeta shM = sh.getItemMeta();
			shM.setDisplayName("�bMorgen");
			sh.setItemMeta(shM);
			
			ItemStack gm = new ItemStack(Material.WATCH);
			ItemMeta gmM = gm.getItemMeta();
			gmM.setDisplayName("�6Mittag");
			gm.setItemMeta(gmM);
			
			ItemStack ti = new ItemStack(Material.WATCH);
			ItemMeta tiM = ti.getItemMeta();
			tiM.setDisplayName("�9Abend");
			ti.setItemMeta(tiM);
			
			kl = Bukkit.createInventory(null, 9, "�4Zeit �ndern");
			kl.setItem(1, sh);
			kl.setItem(4, gm);
			kl.setItem(7, ti);
			return;
		} else {
			return;
		}
	}
	
}
