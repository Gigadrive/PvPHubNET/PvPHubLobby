package net.pvp_hub.lobby.api;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class Locations {

	public static final Location spawn = new Location(Bukkit.getWorld("world"), 188.5376D, 61.0D, -328.4983D, -1.1842F, 17.5007F);
	public static final Location teamSpawn = new Location(Bukkit.getWorld("world"), 279.9322D, 143.5D, -371.3858D, -90.6900F, 4.5800F);
	public static final Location survGames = new Location(Bukkit.getWorld("world"), 309.1346D, 54.0D, -274.4027D, -8904338F, -0.9313F);
	public static final Location gunGame = new Location(Bukkit.getWorld("world"), 1D, 2D, 3D, 4F, 5F);
	public static final Location deathMatch = new Location(Bukkit.getWorld("world"), 340.4871D, 54.0D, -364.4284D, -90.3108F, 1.5263F);
	public static final Location sPvP = new Location(Bukkit.getWorld("world"), 229.7458D, 54.0D, -278.3671D, -0.5698F, -2.1601F);
	public static final Location infWars = new Location(Bukkit.getWorld("world"), 315.4654D, 66.0D, -117.8421D, -2.9993F, 1.9981F);
	public static final Location blockJump = new Location(Bukkit.getWorld("world"), 102.7673D, 54.0D, -230.2809D, 59.6414F, -2.7745F);
	public static final Location raceBow = new Location(Bukkit.getWorld("world"), 161.9605D, 54.0D, -207.8051D, 1.2733F, -21.8209F);
	public static final Location minecartStart = new Location(Bukkit.getWorld("world"), 240.4798D, 54.0D, -302.731D, -179.7824F, 0.9119F);
	
}
