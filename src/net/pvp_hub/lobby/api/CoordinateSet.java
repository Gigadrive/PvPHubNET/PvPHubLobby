package net.pvp_hub.lobby.api;

public class CoordinateSet {
	
	public float x;
	public float y;
	public float z;
	
	public CoordinateSet(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

}