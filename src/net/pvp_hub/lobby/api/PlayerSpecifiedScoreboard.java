package net.pvp_hub.lobby.api;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

public class PlayerSpecifiedScoreboard {

	private Player p;
	private Scoreboard sc;
	private DisplaySlot ds;
	
	private String coinsName = "�6Coins";
	private String chipsName = "�aChips";
	
	public String boardName = "�c�l�oPvP-Hub.net";
	
	public PlayerSpecifiedScoreboard(Player p) {
		this.p = p;
		ds = DisplaySlot.SIDEBAR;
		
		Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        Objective side = scoreboard.registerNewObjective("side", "dummy");
        
        side.getScore(coinsName).setScore(100);
        side.getScore(chipsName).setScore(0);
        
        side.setDisplaySlot(ds);
        side.setDisplayName(boardName);
        
        this.sc = scoreboard;
        this.p.setScoreboard(scoreboard);
	}
    
    public void changeSingle(String scoreName, int value) {
        if(sc != null) {
        	sc.getObjective(ds).getScore(scoreName).setScore(value);
        }
    }

    public void setData(int coins, int chips) {
		changeSingle(coinsName, coins);
		changeSingle(chipsName, chips);
	}
	
}